I am trying to make the rdflib [ResultSerializer.serialize][ResultSerializer.serialize] and [Graph.serialize] methods more consistent. And in doing so I would like to make them operate on both:
 - BinaryIO "like" streams, specifically objects like those returned from `open(...,"wb", buffering=0)` and `open(...,"wb")`.
 - TextIO "like" streams, specifically objects like those returned from `open(...,"w")`.

Some obstacles to solving this:
-

- There currently seems to be a parallel type hierarchy for typing.{IO[...], BinaryIO, TextIO} and io.{TextIOBase, BufferedIOBase, RawIOBase}

[[1][typeshed/stdlib/io.pyi]]

```python
class FileIO(RawIOBase, BinaryIO):
   ...
class BytesIO(BufferedIOBase, BinaryIO):
   ...
```


, this should include [io.RawIOBase][io.RawIOBase] and [io.BufferedIOBase][io.BufferedIOBase]
 but should include [io.TextIOBase][io.TextIOBase]



  [ResultSerializer.serialize]: https://github.com/RDFLib/rdflib/blob/master/rdflib/query.py#L345
  [Graph.serialize]: https://github.com/RDFLib/rdflib/blob/master/rdflib/query.py#L209
  [typeshed/stdlib/io.pyi]: https://github.com/python/typeshed/blob/master/stdlib/io.pyi#L66
  [io.RawIOBase]: https://docs.python.org/3/library/io.html#io.RawIOBase
  [io.BufferedIOBase]: https://docs.python.org/3/library/io.html#io.BufferedIOBase
  [io.TextIOBase]: https://docs.python.org/3/library/io.html#io.TextIOBase

NOTE: Tested for python 3.7.

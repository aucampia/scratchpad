import os

from aucampia.scratchpad.environ import EnvUtils


def test_envmod() -> None:
    assert "HOME" in os.environ
    assert "ABCD" not in os.environ
    with EnvUtils.ctx_setall({"ABCD": "1234", "HOME": None}):
        assert "HOME" not in os.environ
        assert os.environ["ABCD"] == "1234"
    assert "HOME" in os.environ
    assert "ABCD" not in os.environ

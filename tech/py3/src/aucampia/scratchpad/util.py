import inspect
import typing

GenericT = typing.TypeVar("GenericT")


def coalesce(*args: typing.Optional[GenericT]) -> typing.Optional[GenericT]:
    for arg in args:
        if arg is not None:
            return arg
    return None


def vdict(*keys: str, obj: typing.Any = None) -> typing.Dict[str, typing.Any]:
    if obj is None:
        lvars = typing.cast(typing.Any, inspect.currentframe()).f_back.f_locals
        return {key: lvars[key] for key in keys}
    return {key: getattr(obj, key, None) for key in keys}

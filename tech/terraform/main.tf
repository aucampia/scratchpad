resource "null_resource" "none" {
}
resource "tls_private_key" "example" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "local_file" "public_key_pem" {
    content     = tls_private_key.example.public_key_pem
    filename = "${path.module}/output/rsa-example-public.pem"
}

resource "local_file" "private_key_pem" {
    content     = tls_private_key.example.private_key_pem
    filename = "${path.module}/output/rsa-example-private.pem"
}

Name:           rdflib-python
Version:        6.1.0
Release:        1%{?dist}
Summary:        rdflib

License:        BSD-3-Clause
URL:            https://github.com/RDFLib/rdflib
Source0:        https://github.com/RDFLib/rdflib

BuildRequires:  python3-devel
Requires:       python3.8 python3-six python3-isodate

%description


%prep
%autosetup


%build
%configure
%make_build


%install
rm -rf $RPM_BUILD_ROOT
%make_install


%files
%license add-license-file-here
%doc add-docs-here



%changelog
* Tue Dec 21 2021 Iwan Aucamp <aucampia@gmail.com>

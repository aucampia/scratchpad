# ...

```bash
cat filelist.csv | tr ',' '\n' | tr '\n' '\000' | xargs -0 -t -n2 bash -c 'wget -c -O "${2}" "${1}"' /dev/null
```

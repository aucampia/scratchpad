My understanding is that only permission, pertaining to the LGPL-3.0 licensed work, granted by [LGPL-3.0][LGPL-3.0] that is clearly applicable to the arrangement in question is the permission to upgrade to a later version of LGPL (granted in [Section 6][LGPL-3.0#S6]).

All other permissions granted by LGPL-3.0 seems to me to be contingent on conditions that are not present to the arrangement in question (i.e. if an LGPL-3.0 licensed work links against a proprietary work).

The reasoning behind my understanding on this matter follows below.

The question then whether a licensee of an LGPL-3.0 licensed work is permitted convey "modified source versions" or "non-source forms" of said work becomes a question of whether these permissions are granted by [GPL-3.0][GPL-3.0]. On this matter my understanding is that GPL-3.0 does not grant these permissions to licensees, this question and my understanding can be found in this stackexchange question: [Licensing software that uses a proprietary library under GPL-3.0 without linking exceptions: what are the implications?](https://opensource.stackexchange.com/questions/931).

So in summary: My understanding is that neither LGPL-3.0, nor GPL-3.0, grants permission to licensees to convey "modified source versions" or "non-source" versions of an *LGPL-3.0 licensed work* that links against *a proprietary library*.

## LGPL-3.0 definitions and directionality

**LGPL-3.0** defines the following terms [**LGPL-3.0** / Section 0: Additional Definitions.][LGPL-3.0#S0]:

> 1. **“The Library”** refers to a covered work governed by this License, other than **an Application** or a **Combined Work** as defined below.

> 2. **An “Application”** is any work that makes use of **an interface** provided by the **Library**, but which is not otherwise based on **the Library**. Defining a subclass of a class defined by **the Library** is deemed a mode of using an interface provided by **the Library**.

> 3. **A “Combined Work”** is a work produced by combining or linking **an Application** with **the Library**. The particular version of **the Library** with which **the Combined Work** was made is also called the “Linked Version”.

These definitions are in the arrangement that can be seen in this diagram:

[![answer-cmp-lgpl.plantuml][answer-cmp-lgpl.plantuml]][answer-cmp-lgpl.plantuml]

**Figure 1: LGPL-3.0 arrangement ▲**

In this arrangement the following is clearly directional:

> makes use of **an interface** provided by **the Library**

However, in the arrangement in question, there is not something (i.e. another work, proprietary)
«_[making] use of **an interface** provided by **the Library**_».

Instead, in the arrangement in question, there is something (i.e. another work, proprietary) «_providing **an interface** used by **the Library**_»:

[![answer-cmp-question.plantuml][answer-cmp-question.plantuml]][answer-cmp-question.plantuml]

**Figure 2: Arrangement in question ▲**

The arrangement in question differs from the LGPL-3.0 arrangement in that the roles (i.e. provider of interface vs user of interface) of the proprietary work and LGPL-3.0 licensed work are reversed.

The arrangement in question is thus not described by the **LGPL-3.0** definitions and permissions granted by **LGPL-3.0** that are contingent on those definitions are therefore also not granted for the arrangement in question.

Given this, sections can be eliminated for consideration if the permissions they grant are contingent on the LGPL-3.0 definitions or LGPL-3.0 arrangement.

## Eliminated Sections

### [**LGPL-3.0** / Section 1: Exception to Section 3 of the GNU GPL.][LGPL-3.0#S1]

> You may convey a covered work under sections 3 and 4 ...

This is section applies only if section 3 and 4 is applicable and as is shown below those sections are not applicable.

### [**LGPL-3.0** / Section 2: Conveying Modified Versions.][LGPL-3.0#S2]

> If you modify a copy of the Library, and, in your modifications, a facility refers to a function or data to be supplied by **an Application** that uses the facility [...], then you may [...]

The permissions granted by this section are contingent on the definition of **an "Application"**. Since nothing in the arrangement in question satisfies the definition of **an "Application"** the permissions granted by this section are also not in effect.

### [**LGPL-3.0** / Section 3: Object Code Incorporating Material from Library Header Files.][LGPL-3.0#S3]

> The object code form of **an Application** may incorporate material from a header file that is part of **the Library**. You may convey such object code under terms of your choice [...]

Similarly to section 2, The permissions granted by this section are contingent on the definition of **an "Application"**. Since nothing in the arrangement in question satisfies the definition of **an "Application"** the permissions granted by this section are also not in effect.

### [**LGPL-3.0** / Section 4: Combined Works.][LGPL-3.0#S4]

> You may convey **a Combined Work** under terms of your choice [...]

The permissions granted by this section are contingent on the definition **a "Combined Work"**. The definition of a **a "Combined Work"** in turn is contingent on the definition of **an "Application"**.

Since nothing in the arrangement in question satisfies the definition of **an "Application"** there can also not be something in the arrangement in question that satisfies the definition of **a "Combined Work"**. Consequently the permissions granted by this section are not in effect.

## Remaining sections

This elimination process leaves only Section 5 and Section 6:

### [**LGPL-3.0** / Section 5: Combined Libraries.][LGPL-3.0#S5]

> You may place library facilities that are a work based on the Library side by side in a single library together with other library facilities that are not Applications and are not covered by this License, and convey such a combined library under terms of your choice, if [...]

Neither "library facility" nor "side by side" (pertaining to library facilities) are terms of art in software engineering, there is also no clear definition in LGPL-3.0 of these terms. Consequently what would qualify as "side by side" and what would not qualify as "side by side" is somewhat open to interpretation.

There are some snippets from *LGPL-3.0* that we can use to try and piece together what *side by side* means pertaining to *library facilities*:

> [...] place library facilities that are a work based on the Library side by side in a single library [...] ~ [**LGPL-3.0** / Section 5][LGPL-3.0#S5]

> [...] an Application that uses the facility [...] ~ [**LGPL-3.0** / Section 2][LGPL-3.0#S2]

From this we can gather:

 - A library can have one or more *library facility*.
 - Two *library facilities* can be arranged in a manner which we can say they are side-by-side.
 - Two *library facilities* can be arranged in a manner which we can say they are not side-by-side.
 - A *library facility* can be used by something external to the library.

It also seems reasonable:

 - A library facility can use another library facility.

We can now consider the following arrangements of two library facilities:

 - Option A

  [![`answer-cls-facilities_sidebyside_a.plantuml`][answer-cls-facilities_sidebyside_a.plantuml]][answer-cls-facilities_sidebyside_a.plantuml]

  **Figure 3: Option A ▲**

  If we assume *a library facility* to be analogous to *a function* in Python, then we can represent the same in python as:

    ```python
    def lgpl_facility_a():
        pass

    def lgpl_facility_b():
        pass
    ```

  That is, the neither of the two facility uses the other.

 - Option B

  [![`answer-cls-facilities_sidebyside_b.plantuml`][answer-cls-facilities_sidebyside_b.plantuml]][answer-cls-facilities_sidebyside_b.plantuml]

  **Figure 4: Option B ▲**

  In python:

    ```python
    def lgpl_facility_a():
        lgpl_facility_b()

    def lgpl_facility_b():
        pass
    ```

  That is, one of the two facility uses the other.

In these figures "LGPL-3.0 Library Facility" is shorthand for ["library facilities that are a work based on the Library"][LGPL-3.0#S5].

The question now is, does *side to side* refer to the arrangement in Option A or Option B.

My view is that Option A is side by side and Option B is not side by side. Since these terms are not well defined I am sure other views on the meaning of "side by side" is also reasonable.

If we now consider what a ["combined library"][LGPL-3.0#S5] might look as extensions of these options:

 - Combined library: Option A

  [![`answer-cls-facilities_combined_a.plantuml`][answer-cls-facilities_combined_a.plantuml]][answer-cls-facilities_combined_a.plantuml]

  **Figure 5: combined library: Option A ▲**

  In python:

    ```python
    def lgpl_facility_a():
        pass

    def proprietary_facility_a():
        pass

    def lgpl_facility_b():
        pass

    def proprietary_facility_b():
        pass
    ```

  That is, LGPL-3.0 facilities do not use proprietary facilities and proprietary facilities do not use LGPL-3.0 facilities.


 - Combined library: Option B

  [![`answer-cls-facilities_combined_b.plantuml`][answer-cls-facilities_combined_b.plantuml]][answer-cls-facilities_combined_b.plantuml]

  **Figure 6: combined library: Option B ▲**

  In python:

    ```python
    def lgpl_facility_a():
        proprietary_facility_a()

    def proprietary_facility_a():
        pass

    def lgpl_facility_b():
        pass

    def proprietary_facility_b():
        lgpl_facility_b()
    ```

  That is, at least some LGPL-3.0 facilities use proprietary facilities and/or some proprietary facilities use LGPL-3.0 facilities.

From these options, Option B corresponds to the arrangement in question, while Option A does not.

The best answer I can devise to whether this section applies to the arrangement in question (and thus grants any permissions applicable to the arrangement in question) is that it does not clearly apply.

The reason why it does not clearly apply is because "side by side" very well could be interpreted to be Option A only and not Option B - this is how I interpret it.

If the section does not clearly apply, and does not clearly grant any permission, then it should not be assumed to apply either. It could apply, but whether or not it does apply has to be determined by litigation and until such a determination is made the permission cannot be said to be granted.

### [**LGPL-3.0** / Section 6: Revised Versions of the GNU Lesser General Public License.][LGPL-3.0#S6]

This section seems to apply regardless of arrangement, though the only permission it grants it conditional permission to use later versions of LGPL. Subsequent versions of LGPL-3.0 could change the answer but the question is about LGPL-3.0 so other versions will not be considered.

## Views of the FSF

I think the FSF would favour my understanding for the following reasons:

 - The FSF would view a LGPL-3.0 licensed work that links against a proprietary work as "off limits to the Free World":

  > ### Can I write free software that uses nonfree libraries? ([#FSWithNFLibs][GPLFAQ#FSWithNFLibs])

  > If you do this, your program won't be fully usable in a free environment. If your program depends on a nonfree library to do a certain job, it cannot do that job in the Free World. If it depends on a nonfree library to run at all, it cannot be part of a free operating system such as GNU; it is entirely off limits to the Free World.

 - The stated [intent of GPL-3.0 is][GPL-3.0#P]:

  > to guarantee your freedom to share and change all versions of a program--to make sure it remains free software for all its users.

 - The stated [intent of LGPL-3.0 is][GNULR#libraries]:

  > The Lesser GPL was designed to fill the middle ground between these cases, allowing proprietary software developers to use the covered library, but providing a weak copyleft that gives users freedom regarding the library code itself.

Given this, it seems likely that LGPL-3.0 was not intended to allow "proprietary software developers" to turn free software into something "off limits to the Free World". And this would be permitted if licensees are allowed to convey "modified source versions" and "non-source forms" as they could take a "free software" library, enhance it but make the enhancements depend on proprietary software thus making it "off limits to the Free World".

This kind of thing also seems like exactly what [**LGPL-3.0** / Section 2: Conveying Modified Versions.][LGPL-3.0#S2] is trying to prevent with conditions in subsection b.

  [GNULR#libraries]: https://www.gnu.org/licenses/license-recommendations.html#libraries
  [GPLFAQ#FSWithNFLibs]: https://www.gnu.org/licenses/gpl-faq.en.html#FSWithNFLibs
  [GPL-3.0]: https://www.gnu.org/licenses/gpl-3.0-standalone.html
  [GPL-3.0#P]: https://www.gnu.org/licenses/gpl-3.0-standalone.html#preamble
  [LGPL-3.0]: https://www.gnu.org/licenses/lgpl-3.0-standalone.html
  [LGPL-3.0#S0]: https://www.gnu.org/licenses/lgpl-3.0-standalone.html#section0
  [LGPL-3.0#S1]: https://www.gnu.org/licenses/lgpl-3.0-standalone.html#section1
  [LGPL-3.0#S2]: https://www.gnu.org/licenses/lgpl-3.0-standalone.html#section2
  [LGPL-3.0#S3]: https://www.gnu.org/licenses/lgpl-3.0-standalone.html#section3
  [LGPL-3.0#S4]: https://www.gnu.org/licenses/lgpl-3.0-standalone.html#section4
  [LGPL-3.0#S5]: https://www.gnu.org/licenses/lgpl-3.0-standalone.html#section5
  [LGPL-3.0#S6]: https://www.gnu.org/licenses/lgpl-3.0-standalone.html#section6

  [answer-cls-facilities_combined_a.plantuml]: https://www.plantuml.com/plantuml/svg/bL71Yi8m4BttAmOzU79nMRo82csnUn5MMFQoU4dJiGscIPb4f2pxxvlQogPh1JUdvCtBUszc91uiM0qayi929WYv5Le5vYXKnOYLO0gMwRDGEHoOD1WK8aF0ih8rC6j9f2UBnaTBj8NEEeWBuhBxP2mIQbjNwCbNZ1zPZk-sbWYDErEv--0nYAcZmGgUvpCVvf8P0ry1k1Dfof1YBJN1U7Ov5pmaqkTRE7MUEUcJoYwrvV9rjvs-FCtMQnZ7yMQU9CEqRO-N974ypDkHhaYWPLGtr8OuBDdMdCnt4BJzR9ImzISm0wFLQgDiyne5GJUb1kIwJ8N23AH8oJaiuArgLWRXNZcCepAuW4cjJmIyq0wOZBenjHRRl989uq8wxR1pkdQsL-xUwiAL0-48_DuzAQ_HlufUwRveRr1t8aQFH8p-4p5wC69q6z7jgTlGTBhkJt817mODh7t2q7o6UBr-7nJwvVq0
  [answer-cls-facilities_combined_b.plantuml]: https://www.plantuml.com/plantuml/svg/bL71Yi8m4BtdAuPwyEGYB7iHBTXYzoAiiEnbyPAcOnjCapA9I5dstpUrrQrk1JUdvCtBcpTl5EuSE0Cayy919Wc5aqOpkvUwuiHBi0NFp57g77PSMMI5p12mh5mDt3cIwS6XxQCbki9a7IGa2TLziWu9ZQihxCbNNEnvZk-kLWXDTwvp_u7rA49nQ-6BWJ-HeGmfDieGZAQdSyANHERu5qwzUaxceBDJRJv_tQmdpq_JC8HH7AzUacIOjhxY9KaS3_CsP2gIw3ZL3RKX3aksDI_ppLWxpqfBLty2jn0i5YljcbV0M9T70mfJfb9Z1agcv3lCuArgbWFHLdjCeBOuWt5j3WIYC1uO1rrCROjrNgc42wcyzhBhT9vig_szrOKp1vO1z6VlIVK6lLNibUwBNWLrnsBqYCNeFnQZ1or6VotwFNKRcap2woHdy67HGiYkysruGp5Tw3sz8QyNtk0Wjov-wJy0
  [answer-cls-facilities_sidebyside_a.plantuml]: https://www.plantuml.com/plantuml/svg/ZP51Qy8m5CVl_HGFlHKSZk55jD0MkqjXmhRBy1BRPnkCINXv8cNiko-rTTHDOJab__poUtc9mXq36o1PrGob92nO6XsulTHMa3Y0gqLfJb9Ni1FAOL3B4W4FbXiGp2ItHqOtJ0_8jIdxg91KgFwGOoGqt5WSwAqezgB2LsuKGbjTwCeV610azDwXXSVPT1WNIZW7dm7uaHWgaLAZ3C5uUXxdF2Oofx_nrjUio1nrUTvRB9xN-UJfOHf5C4xJrIpBRcFv5PTbQNgRMvEn9945DItQWhULtPxNV0L1ryzAIsu-G3WOBPShpUrg50JzAxNXMusGoorv-HnURFjR46yqA5cWTZY7CBpSCmn7_GDryhooAXE5LDuQzpKkx4RxUQU52mFn28PTtr4b_r4blrI-uxxNoIIwljaStXqwYEu1oG_m3G00
  [answer-cls-facilities_sidebyside_b.plantuml]: https://www.plantuml.com/plantuml/svg/ZP51Qy8m5CVl-HIFUYiuXB6BQC6MTfV2X6sNuIMsppOOa_BoHChOTrzgwwZEmN9A_ldbb_SIZJi6ja2ggXbAHLYmiaQulJAD97a0LylIdfIfO2UrGr6h4W4F3RSWcKbjZunkd1wGQriEKQ6eqCCXnqXekMrmf6zaiPSLldAh4RhRfQdy0J522YsTWqy1ViIMIgJ4QaimcPt7ELyHsTFlUElj5TcZASzxYyNpEfy-FioY22P9aZvbsNqil-AoB4dkSsko3IbaIMs7Tk1zPR_dDLz2zFsaHd7x0T91i5ocXhjL8CJm7btuLYFaQajUFeUNfliNY3S6j2hGE9n365xg3CDWUA1UdbUDpcIXjBUkXZikxCRuUQ-52mEh0CPT_w6A_wEARrI-uw7NwJIwhcmExmuTHEAwtbik_k6-0G00
  [answer-cmp-lgpl.plantuml]: https://www.plantuml.com/plantuml/svg/TPB1Qi9048RlUOf1B-s2GLfw4H7IK4l1Wz12BxsioPWi9hlBxbgH8lGfsZVf0_aadMYY6vlEQVddc_bdXir5Yc0T6bHkgp6mAo4rDxo4H9LQIPJksMqBX7LPt7XPc_DKROJCOCaBwzS9ITsMFC5xdgmoezOoZLMX3FIdX-X4bKdHrDZm447NAF0H04KNNkcHCMhpLrwq1ncF7-UpmMqud4oW7ySFTpHJ9pPhSTDf77TpSwEq4UYusLPe1LBBNH1e8dc6y99ZhCg5a9Y-AhC2Rg776AdGo53fZFKE2vxN9MGc5eOQUpMrmXYyIRZY4feveH09IalTDjJjcj84rDe866kuoiVVhcKPoKXhAk9EA5cxHX8yiJ46VTxx7qVlCaVJrgd8z_G3oFVfz3wwqcQdD5K7X7lt61IuT8DMsGZstv_LLE_qgItXxLt2q8WiTs4ukQXQMxIm__e9Thy0
  [answer-cmp-question.plantuml]: https://www.plantuml.com/plantuml/svg/RPB1QiCm38RlVWhHNZP3GzdOfPH25zOnwA6ZE-xY9cfYcjZ1TbRAAEmfjZVP0_L9fdHf6s_MoKZVB_qIpcMAO1qQr6vN8RCRgIfXH0c9BYkjKBcbsnK8TLdSTAes5wdUIfN1MXIsgvEAkgr5WlSYsMH6rogDTQ4Dz6V72ABQf6XQR7WC5h82xmmeGdYJdngZj__JAz_8UFouc0zke-5a0lquVhWZJq5ixd6pMHo7kONHbP7eXDarQ0DIoprZ5P4YGrZg4XVjMrZeSKvEXSgmR7QI2fvhj4vgnNdlkE9buPSSvt9bg7Mhf0Iq6SxXLS6LK71o2eLCK5bCOQlDvfhA1BIv4N1-ufevtVsyaTFEFlvi0TMbH7qz5rujFBxJ-DU1TpaoSVuGC5LFvtSxN5tAf6Q4XvmE2boxWQSVmU7hex7tHf_Tagvpr2WoCijT54t-I6gB5WwVtsp_0m00


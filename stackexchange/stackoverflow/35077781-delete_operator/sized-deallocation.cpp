void* operator new(long unsigned int howMuch) {
    return reinterpret_cast<void*>(0xdeadbeef);
}

void* operator new[](long unsigned int howMuch) {
    return reinterpret_cast<void*>(0xdeadbeef);
}

void operator delete(void* what) {
    if(what != reinterpret_cast<void*>(0xdeadbeef)) __builtin_trap();
}

void operator delete(void* what, long unsigned int howmuch) {
    if(what != reinterpret_cast<void*>(0xdeadbeef)) __builtin_trap();
    if(howmuch != 1) __builtin_trap();
}

extern "C"
void _start() {
    char* ptr = new char;
    delete ptr;
    operator delete(ptr);
    operator delete(ptr, 1);
    asm("syscall" : : "a"(60) : ); 
}

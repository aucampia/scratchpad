import logging
import sys
import re
import os

logging.basicConfig(
    level=os.environ.get("PYTHON_LOGGING_LEVEL", logging.INFO),
    stream=sys.stderr,
    datefmt="%Y-%m-%dT%H:%M:%S",
    format=(
        "%(levelname)-8s %(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s"
    ),
)

# Sample Output - stored in list crypto_map_details.
data = """\
crypto map outside_map 65535 ipsec-isakmp dynamic dynamic_map
crypto map outside_map interface outside
crypto map inside_map 1 match address asa->ftd
crypto map inside_map 1 set pfs
crypto map inside_map 1 set peer 10.254.254.7
crypto map inside_map 1 set ikev1 transform-set AES256-SHA
crypto map inside_map 1 set security-association lifetime seconds 3600
crypto map inside_map interface inside"""


crypto_map_details = data.splitlines()
crypto_map_entries = []

for index, line in enumerate(crypto_map_details):
    logging.info("processing %s:%s", index, line)
    # Collecting data about a crypto map
    # Output for a crypto map entry starts with a unique number.
    if "crypto map" in line and "interface" not in line:
        logging.info("non interface")
        try:
            match = re.match(
                r"crypto map (?P<map_name>.+?(?=\s)) (?P<map_seq>\d{1,5})", line
            )
            crypto_map_seq = int(match.group("map_seq"))
            crypto_map_name = match.group("map_name")
        except:
            logging.warning("got exception ...", exc_info=True)
        logging.info(
            f"Collecting data for crypto map {crypto_map_name} entry: {crypto_map_seq}."
        )
    count = index
    crypto_map_entry_details = []

    # Add details about the crypto map until the next crypto map sequence number is found.
    try:
        # Find the next line's sequence number.
        match = re.match(
            r"crypto map (?P<map_name>.+?(?=\s)) (?P<seq>\d{1,5})",
            crypto_map_details[count + 1],
        )
        if match is not None:
            next_line_seq = match.group("seq")
            # Loop through the following lines until the crypto map sequence number changes.
            while int(next_line_seq) == crypto_map_seq:
                crypto_map_entry_details.append(crypto_map_details[count])
                count += 1
                if len(crypto_map_details) == count + 2:
                    crypto_map_entry_details.append(crypto_map_details[count + 1])
                    break
            # Add the VPN tunnel peer and details to the list.
            crypto_map_entries.append(
                {
                    "crypto_map_seq": crypto_map_seq,
                    "crypto_map_details": crypto_map_entry_details,
                }
            )
    except Exception as err:
        logging.error("got exception ...", exc_info=True)

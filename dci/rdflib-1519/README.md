# ...
```bash
rpmdev-setuptree

cd /home/devuser/rdflib && python3.8 -Bm build -w

rm -rf ~/rpmbuild/BUILDROOT/python-rdflib-6.1.0-2.fc35.x86_64/
mkdir -vp ~/rpmbuild/BUILDROOT/python-rdflib-6.1.0-2.fc35.x86_64/
python3.8 -m pip install \
    --root ~/rpmbuild/BUILDROOT/python-rdflib-6.1.0-2.fc35.x86_64/ \
    --prefix /usr/ \
    /home/devuser/rdflib/dist/rdflib-6.1.1-py3-none-any.whl


export PYTHONPATH=${HOME}/rpmbuild/BUILDROOT/python-rdflib-6.1.0-2.fc35.x86_64/usr/lib/python3.8/site-packages:${HOME}/rpmbuild/BUILDROOT/python-rdflib-6.1.0-2.fc35.x86_64/usr/lib/python3.8/site-packages

cd /home/devuser/rdflib && python3.8 -m pytest -ra
```

### trash

```
mkdir /home/devuser/rdflib-rpm
cd /home/devuser/rdflib-rpm
```


```
https://grimoire.carcano.ch/blog/packaging-a-python-wheel-as-rpm/

PYTHONPATH=${HOME}/rpmbuild/BUILDROOT/python-rdflib-6.1.0-2.fc35.x86_64/usr/lib/python3.8/site-packages:${HOME}/rpmbuild/BUILDROOT/python-rdflib-6.1.0-2.fc35.x86_64/usr/lib/python3.8/site-packages

python3-pytest -ra
```

```
update-alternatives --install
update-alternatives \
    --install /usr/bin/python python /usr/bin/python3.8 1 \
    --slave /usr/bin/python3 python3 /usr/bin/python3.8 \
```

```
rpmdev-newspec rdflib-python

```

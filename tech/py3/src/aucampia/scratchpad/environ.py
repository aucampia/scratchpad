import os
from contextlib import contextmanager
from typing import Dict, Iterator, Mapping, Optional


class EnvUtils:
    @classmethod
    def set(cls, key: str, value: Optional[str]) -> Optional[str]:
        original = os.environ.get(key, None)
        if value is None:
            del os.environ[key]
        else:
            os.environ[key] = value
        return original

    @classmethod
    def setall(cls, envmod: Mapping[str, Optional[str]]) -> Mapping[str, Optional[str]]:
        backups: Dict[str, Optional[str]] = {}
        for key, value in envmod.items():
            backup = backups[key] = os.environ.get(key, None)
            if value is None and backup is not None:
                del os.environ[key]
            elif value is not None:
                os.environ[key] = value
        return backups

    @classmethod
    @contextmanager
    def ctx_setall(cls, envmod: Mapping[str, Optional[str]]) -> Iterator[None]:
        backup = cls.setall(envmod)
        try:
            yield None
        finally:
            cls.setall(backup)

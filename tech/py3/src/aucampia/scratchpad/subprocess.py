import abc
import inspect
import logging
import shlex
import subprocess
import sys
import textwrap
from collections import OrderedDict
from dataclasses import dataclass, field
from os import PathLike
from subprocess import CompletedProcess
from typing import (
    IO,
    Any,
    Callable,
    Dict,
    List,
    Mapping,
    Optional,
    Protocol,
    Sequence,
    Set,
    TextIO,
    Union,
    cast,
    overload,
)

from pydantic import create_model_from_typeddict
from typing_extensions import Literal, TypedDict

# https://github.com/python/typeshed/blob/master/stdlib/subprocess.pyi
# https://github.com/python/typeshed/blob/master/stdlib/os/__init__.pyi

StrOrBytesPath = Union[str, bytes, "PathLike[str]", "PathLike[bytes]"]  # stable

FileOption = Union[None, int, IO[Any]]
BytesOrString = Union[bytes, str]
ArgsT = Union[BytesOrString, Sequence[StrOrBytesPath]]
EnvironT = Mapping[str, str]
CompletedProcessAny = Union["CompletedProcess[str]", "CompletedProcess[bytes]"]


def get_argdict(exclude: Optional[Set[str]] = None) -> Dict[str, Any]:
    frame = inspect.getouterframes(inspect.currentframe())[1].frame
    assert frame
    args, _, _, values = inspect.getargvalues(frame)
    arg_dict = OrderedDict()
    for arg in args:
        if exclude is not None and arg in exclude:
            continue
        arg_dict[arg] = values[arg]
    return arg_dict


def args_to_str(args: ArgsT) -> str:
    if isinstance(args, bytes):
        return args.decode()
    elif isinstance(args, str):
        return args
    parts: List[str] = []
    for arg in args:
        if isinstance(arg, bytes):
            quoted_arg = shlex.quote(arg.decode())
        elif isinstance(arg, str):
            quoted_arg = shlex.quote(arg)
        else:
            quoted_arg = shlex.quote(f"{arg}")
        parts.append(quoted_arg)
    return " ".join(parts)


class RunArgsDict(TypedDict):
    args: ArgsT
    bufsize: int
    executable: Optional[StrOrBytesPath]
    stdin: FileOption
    stdout: FileOption
    stderr: FileOption
    preexec_fn: Optional[Callable[[], Any]]
    close_fds: bool
    shell: bool
    cwd: Optional[StrOrBytesPath]
    env: Optional[EnvironT]
    universal_newlines: bool
    # universal_newlines: Optional[bool]
    startupinfo: Any
    creationflags: int
    restore_signals: bool
    start_new_session: bool
    pass_fds: Any
    check: bool
    encoding: Optional[str]
    errors: Optional[str]
    input: Optional[Union[str, bytes]]
    timeout: Optional[float]


def make_run_args_dict(
    args: ArgsT,
    bufsize: int = -1,
    executable: Optional[StrOrBytesPath] = None,
    stdin: FileOption = None,
    stdout: FileOption = None,
    stderr: FileOption = None,
    preexec_fn: Optional[Callable[[], Any]] = None,
    close_fds: bool = True,
    shell: bool = False,
    cwd: Optional[StrOrBytesPath] = None,
    env: Optional[EnvironT] = None,
    universal_newlines: bool = False,
    startupinfo: Any = None,
    creationflags: int = 0,
    restore_signals: bool = True,
    start_new_session: bool = False,
    pass_fds: Any = (),
    *,
    check: bool = False,
    encoding: Optional[str] = None,
    errors: Optional[str] = None,
    input: Optional[BytesOrString] = None,
    timeout: Optional[float] = None,
) -> RunArgsDict:
    arg_dict = cast(RunArgsDict, get_argdict({"self"}))
    _ = RunArgsModel.parse_obj(arg_dict)
    return arg_dict


class RunArgsModelConfig:
    extra = "forbid"
    arbitrary_types_allowed = True


RunArgsModel = create_model_from_typeddict(RunArgsDict, __config__=RunArgsModelConfig)


class SubprocessHandler(Protocol):
    def run(self, args: RunArgsDict) -> CompletedProcessAny:
        ...


class AbstractSubprocessHandler(SubprocessHandler, abc.ABC):
    @classmethod
    def make(
        cls, logging: bool = False, echo: bool = False, dedent: bool = False
    ) -> SubprocessHandler:
        result: SubprocessHandler = cls()
        if logging:
            result = LogSubprocessHandler(result)
        if echo:
            result = EchoSubprocessHandler(result)
        if dedent:
            result = DedentSubprocessHandler(result)
        return result

    @classmethod
    def facade(
        cls, logging: bool = False, echo: bool = False, dedent: bool = False
    ) -> "SubprocessFacade":
        handler = cls.make(logging=logging, echo=echo, dedent=dedent)
        return SubprocessFacade(handler=handler)


class OSSubprocessHandler(AbstractSubprocessHandler):
    def run(self, args: RunArgsDict) -> CompletedProcessAny:
        result = subprocess.run(**args)
        return result


class PretendSubprocessHandler(AbstractSubprocessHandler):
    def run(self, args: RunArgsDict) -> CompletedProcessAny:
        return CompletedProcess(args["args"], 0, "", "")


@dataclass
class DedentSubprocessHandler(SubprocessHandler):
    handler: SubprocessHandler

    def run(self, args: RunArgsDict) -> CompletedProcessAny:
        if isinstance(args["args"], str):
            args["args"] = textwrap.dedent(args["args"])
        return self.handler.run(args)


@dataclass
class EchoSubprocessHandler(SubprocessHandler):
    handler: SubprocessHandler
    sink: TextIO = field(default_factory=lambda: sys.stderr)

    def run(self, args: RunArgsDict) -> CompletedProcessAny:
        args_str = args_to_str(args["args"])
        self.sink.write(f"{args_str}\n")
        return self.handler.run(args)


@dataclass
class LogSubprocessHandler(SubprocessHandler):
    handler: SubprocessHandler
    logger: logging.Logger = field(default_factory=lambda: logging.getLogger())

    def run(self, args: RunArgsDict) -> CompletedProcessAny:
        args_str = args_to_str(args["args"])
        self.logger.info("running %s", args_str)
        return self.handler.run(args)


@dataclass(frozen=True)
class SubprocessFacade:
    handler: SubprocessHandler

    # Nearly same args as Popen.__init__ except for timeout, input, and check
    @overload
    def run(
        self,
        args: ArgsT,
        bufsize: int = ...,
        executable: Optional[StrOrBytesPath] = ...,
        stdin: FileOption = ...,
        stdout: FileOption = ...,
        stderr: FileOption = ...,
        preexec_fn: Optional[Callable[[], Any]] = ...,
        close_fds: bool = ...,
        shell: bool = ...,
        cwd: Optional[StrOrBytesPath] = ...,
        env: Optional[EnvironT] = ...,
        universal_newlines: bool = ...,
        # universal_newlines: Optional[bool] = ...,
        startupinfo: Any = ...,
        creationflags: int = ...,
        restore_signals: bool = ...,
        start_new_session: bool = ...,
        pass_fds: Any = ...,
        *,
        check: bool = ...,
        encoding: str,
        errors: Optional[str] = ...,
        input: Optional[str] = ...,
        timeout: Optional[float] = ...,
    ) -> "CompletedProcess[str]":
        ...

    @overload
    def run(
        self,
        args: ArgsT,
        bufsize: int = ...,
        executable: Optional[StrOrBytesPath] = ...,
        stdin: FileOption = ...,
        stdout: FileOption = ...,
        stderr: FileOption = ...,
        preexec_fn: Optional[Callable[[], Any]] = ...,
        close_fds: bool = ...,
        shell: bool = ...,
        cwd: Optional[StrOrBytesPath] = ...,
        env: Optional[EnvironT] = ...,
        universal_newlines: bool = ...,
        startupinfo: Any = ...,
        creationflags: int = ...,
        restore_signals: bool = ...,
        start_new_session: bool = ...,
        pass_fds: Any = ...,
        *,
        check: bool = ...,
        encoding: Optional[str] = ...,
        errors: str,
        input: Optional[str] = ...,
        timeout: Optional[float] = ...,
    ) -> "CompletedProcess[str]":
        ...

    @overload
    def run(
        self,
        args: ArgsT,
        bufsize: int = ...,
        executable: Optional[StrOrBytesPath] = ...,
        stdin: FileOption = ...,
        stdout: FileOption = ...,
        stderr: FileOption = ...,
        preexec_fn: Optional[Callable[[], Any]] = ...,
        close_fds: bool = ...,
        shell: bool = ...,
        cwd: Optional[StrOrBytesPath] = ...,
        env: Optional[EnvironT] = ...,
        *,
        universal_newlines: Literal[True],
        startupinfo: Any = ...,
        creationflags: int = ...,
        restore_signals: bool = ...,
        start_new_session: bool = ...,
        pass_fds: Any = ...,
        # where the *real* keyword only args start
        check: bool = ...,
        encoding: Optional[str] = ...,
        errors: Optional[str] = ...,
        input: Optional[str] = ...,
        timeout: Optional[float] = ...,
    ) -> "CompletedProcess[str]":
        ...

    @overload
    def run(
        self,
        args: ArgsT,
        bufsize: int = ...,
        executable: Optional[StrOrBytesPath] = ...,
        stdin: FileOption = ...,
        stdout: FileOption = ...,
        stderr: FileOption = ...,
        preexec_fn: Optional[Callable[[], Any]] = ...,
        close_fds: bool = ...,
        shell: bool = ...,
        cwd: Optional[StrOrBytesPath] = ...,
        env: Optional[EnvironT] = ...,
        universal_newlines: Literal[False] = ...,
        startupinfo: Any = ...,
        creationflags: int = ...,
        restore_signals: bool = ...,
        start_new_session: bool = ...,
        pass_fds: Any = ...,
        *,
        check: bool = ...,
        encoding: None = ...,
        errors: None = ...,
        input: Optional[bytes] = ...,
        timeout: Optional[float] = ...,
    ) -> "CompletedProcess[bytes]":
        ...

    def run(
        self,
        args: ArgsT,
        bufsize: int = -1,
        executable: Optional[StrOrBytesPath] = None,
        stdin: FileOption = None,
        stdout: FileOption = None,
        stderr: FileOption = None,
        preexec_fn: Optional[Callable[[], Any]] = None,
        close_fds: bool = True,
        shell: bool = False,
        cwd: Optional[StrOrBytesPath] = None,
        env: Optional[EnvironT] = None,
        universal_newlines: bool = False,
        startupinfo: Any = None,
        creationflags: int = 0,
        restore_signals: bool = True,
        start_new_session: bool = False,
        pass_fds: Any = (),
        *,
        check: bool = False,
        encoding: Optional[str] = None,
        errors: Optional[str] = None,
        input: Optional[BytesOrString] = None,
        timeout: Optional[float] = None,
    ) -> CompletedProcessAny:
        arg_dict = cast(RunArgsDict, get_argdict({"self"}))
        _ = RunArgsModel.parse_obj(arg_dict)
        return self.handler.run(arg_dict)

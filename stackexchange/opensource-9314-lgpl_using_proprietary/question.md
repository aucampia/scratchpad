# What permissions does LGPL-3.0 grant if the licensed work links against a proprietary library?

According to the FSF ["The Lesser GPL permits use of the library in proprietary program"][WNL].
Is this permission granted in cases where an LGPL-3.0 licensed work links against a proprietary library?

Both [GPL-3.0][GPL-3.0] and [LGPL-3.0][LGPL-3.0] grants specific
permissions ("rights") from a copyright holder of a work to other parties or *licensees*.
Some of the permissions granted by these licenses have specific conditions
which must be met for the permission grant to be in effect.
If these conditions are not met then permissions contingent on them are also not granted.

This question seeks to clarify whether the requisite conditions
for any of the permissions granted by LGPL-3.0 are met if an LGPL-3.0 licensed work
links against a proprietary library.

Specifically is a *licensee* of an LGPL-3.0 licensed work that links against a
proprietary library permitted do any of the following with the LGPL-3.0 licensed work:

 - [convey modified source versions](GPL-3.0#S4) of the LGPL-3.0 licensed work.
 - [convey non-source forms](GPL-3.0#S5) of the LGPL-3.0 licensed work.

And if so, under what conditions and are these permission to do this granted by LGPL-3.0 (as opposed to GPL-3.0)

The arrangement in question can be represented visually as:

[![`question-cls-x_lgpl-l_propr.plantuml`][question-cls-x_lgpl-l_propr.plantuml]][question-cls-x_lgpl-l_propr.plantuml]

This implies that **the LGPL-3.0 licensed work** uses an interface provided by the **the proprietary library**:

[![`question-cmp-x_lgpl-l_propr.plantuml`][question-cmp-x_lgpl-l_propr.plantuml]][question-cmp-x_lgpl-l_propr.plantuml]

For examples of how the arrangement in question can occur, consider the following examle arrangements:

 - Example Arrangement 1: `ApplicationA {Proprietary}` links against licensed `LibraryD {LGPL-3.0}` which links against `LibraryE {Proprietary}`:

  [![`question-cls-a_propr-l_lgpl-l_propr.plantuml`][question-cls-a_propr-l_lgpl-l_propr.plantuml]][question-cls-a_propr-l_lgpl-l_propr.plantuml]

  This implies that `ApplicationA {Proprietary}` uses an interface provided by `LibraryD {LGPL-3.0}`,
  and that `LibraryD {LGPL-3.0}` uses an interface provided by `LibraryE {Proprietary}`:

  [![`question-cmp-a_propr-l_lgpl-l_propr.plantuml`][question-cmp-a_propr-l_lgpl-l_propr.plantuml]][question-cmp-a_propr-l_lgpl-l_propr.plantuml]

 - Example Arrangement 2: `ApplicationB {LGPL-3.0}` links against `LibraryD {LGPL-3.0}` which links against `LibraryE {Proprietary}`:

  [![`question-cls-a_lgpl-l_lgpl-l_propr.plantuml`][question-cls-a_lgpl-l_lgpl-l_propr.plantuml]][question-cls-a_lgpl-l_lgpl-l_propr.plantuml]

  This implies that `ApplicationB {LGPL-3.0}` uses an interface provided by `LibraryD {LGPL-3.0}`,
  and that `LibraryD {LGPL-3.0}` uses an interface provided by `LibraryE {Proprietary}`:

  [![`question-cmp-a_lgpl-l_lgpl-l_propr.plantuml`][question-cmp-a_lgpl-l_lgpl-l_propr.plantuml]][question-cmp-a_lgpl-l_lgpl-l_propr.plantuml]

 - Example Arrangement 3: `ApplicationC {LGPL-3}` links against `LibraryE {Proprietary}`

  [![`question-cls-a_lgpl-l_propr.plantuml`][question-cls-a_lgpl-l_propr.plantuml]][question-cls-a_lgpl-l_propr.plantuml]

  This implies that `ApplicationC {LGPL-3}` uses an interface provided by `LibraryE {Proprietary}`:

  [![`question-cmp-a_lgpl-l_propr.plantuml`][question-cmp-a_lgpl-l_propr.plantuml]][question-cmp-a_lgpl-l_propr.plantuml]

So the question applied to these examples arrangements:

 - Example Arrangement 1 and 2:

  Does LGPL-3.0 grant a *licensee* permission to convey "modified source versions" or "non-source forms" of `LibraryD {LGPL-3}`.
  Further, are there any other permissions, pertaining to `LibraryD`, that LGPL-3.0 grants that are not already granted by GPL-3.0

 - Example Arrangement 2:

  Does LGPL-3.0 grant a *licensee* permission to convey "modified source versions" or "non-source forms" of `ApplicationC {LGPL-3}`.
  Further, are there any other permissions, pertaining to `ApplicationC`, that LGPL-3.0 grants that are not already granted by GPL-3.0.

In this question "proprietary library" refers to a library that is
NOT licensed under an LGPL-3.0 and GPL-3.0 compatible license and which, additionally, is
NOT licensed under a license that is considered an open source or free software license.

No other licenses than those explicitly mentioned here should be considered in the answer,
this includes open source licenses on proprietary works (they would not be proprietary if such licenses existed)
or exceptions such as linking exceptions.

My current understanding is that is that at least some permissions granted by LGPL-3.0
are contingent on directionality of linking as a consequence of the
[definitions used by LGPL-3.0](https://www.gnu.org/licenses/lgpl-3.0-standalone.html#section0), such as:

> A “Combined Work” is a work produced by combining or **linking an Application with the Library**.

And that those permissions would not be granted in any of the example arrangements.

  [WNL]: https://www.gnu.org/licenses/why-not-lgpl.html
  [GPL-3.0]: https://www.gnu.org/licenses/gpl-3.0-standalone.html
  [LGPL-3.0]: https://www.gnu.org/licenses/lgpl-3.0-standalone.html
  [GPL-3.0#S4]: https://www.gnu.org/licenses/gpl-3.0-standalone.html#section4
  [GPL-3.0#S5]: https://www.gnu.org/licenses/gpl-3.0-standalone.html#section5
  [LGPL-3.0#S0]: https://www.gnu.org/licenses/lgpl-3.0-standalone.html#section0
  [question-cls-a_lgpl-l_lgpl-l_propr.plantuml]: https://www.plantuml.com/plantuml/svg/dP1DRi8m48NNzIb6i8i4GggwGH09qgGR5bp194CoYcDRuq48eTwzIO0s4QmwEx_t_CsFmQE0E60gAu6267CXPrMeoNhDke5GwSAToPPmr2QWggX0mCRB1RG8q-4a68PgWrAvuYxbnBcvVmg2Z4uk7jM0dniT0bmLjBLrN20dpZY6wQAlNjymk_EpVD1vNR8xsQBtLgkl_MxsFb_4CKoJ9Ft8ijUntIYNPKdoEhTdvvbGD5-wQ1TiaTzAtMRUU6yerzszjg03JDRheJI1KQUhUcijj06N44KF8ufQtWstemEtdJxlgCVpdvXqZ4axp7YVCMdWzJ0rscusY__cMu8XMuCkDTaW4AjVPn1Bds8_
  [question-cls-a_lgpl-l_propr.plantuml]: https://www.plantuml.com/plantuml/svg/RP11QuD048NFtLyn93SX9L1w2OcGBh4N3_aBgqvqSDsLsGb1GlzxrHYgD7FRzzv-ynYB5m7nm5HM0WKnvaBUgL2JQmsR1a9b2dyZLyB5s82geW81crOwC29CsLKmpDK6fVB59EN4kPq-1K56BrsBQiRFhGa1xWhw-V9S86jlFSDwEywe7vdzxR-ScRmksLzTCNhx_VSvtNoyR-COrbgVFfFaTInTv994wzUvC_kM2SLmDqI7O8_yKUhH-TYsbd8pt4k32R0w7ERI2XQRxkgjjz05t44KFOqewdaFN4ePzvjE4-hv733B8alIp1jXQb5hiud_m3kmv6emfI4N1E9V
  [question-cls-a_propr-l_lgpl-l_propr.plantuml]: https://www.plantuml.com/plantuml/svg/dP3DReCm48JlVefLSaEYYbJr4YL82OLUEEGL36p0mjXelL64ghvx-KiB9QVkpJEpdsQjySh05aWL9KEk23DMrWXNATD8aZMuKkRsham1LwaTYbBb25Wtt89a9fNU6DrShP5BcqzIfYZJqv9Z9BJSDYXc_4nBv-1JG3TdIpbIQBKbsEw66VGJaRslvLHcLK7sPlB1Enm-Bed_-h8B0jY6OVGMnyzZoI8Nns7uF7SXsv12bjJsqJxO8R-463kVcaQhJFR_TGBfO7CypgKD_2MCHtO1D0xtu7aps_CwyCXDL4gTyZun7y-UDsk-WZsyDIbQag9_D1EBOtq_-6svx_PC1RAGoZY6GFmuirYqZdq3
  [question-cls-x_lgpl-l_propr.plantuml]: https://www.plantuml.com/plantuml/svg/RO-nQiCm48RdwYcEP3Eu14gNa1XQKtVnaAspObtimx9aJXU22Nttsh53RH8jWlj-VV_9uab0F32LbO0XnaB8EnLgSgrctK2ejF4NSYMSj0seAZ88sBJIWHPXEfu5mtpQe5JUJAE2kB3Jeo38wALhKStyXTKXm5L1Vpuz6-JKMy-mtjpERVx1x2-FuwCkwfBzsPaRs-s-3tdyzhf94bYdwTTxbZsFvOjSbgNfyzo1VSk4ehaReaEmL_ugDUuyQ78gq0KqFvvhq05M-_tT3dS60rp1elUgNko8jn15Tn15__PPUKv7xg_9FmCmaK6-N7Rfdx6nuc7rE4wUDMx1aki_LcfoGI3v0m00
  [question-cmp-a_lgpl-l_lgpl-l_propr.plantuml]: https://www.plantuml.com/plantuml/svg/ZPB1Qi9048Rl-nI3tW89qjABgA19B0KFGg-zhCbe5fFTPNQjI2dqATetwGFv95sjZTacXSvjP__v_Z-pAKIEO2mIAdlGoCnMICs9Lv2fIYk9qZxQGucmgygRngqfUAxsGcvWpKlJd1FIqTOymndFjXjIEvd7gbG4_VcvEgMASgIBR70krXK5NXYuwfAV-bCYjM-tLtwGqUX-kGXley5a0lquJkzSfaxPmjFDvt7ShLkIqYJGSZgSf2UXGxuoTiqzrReK6RT2oHbm0xtns6izIN07b0Q740G_na7GWsQC1d0XLkJyaWkiFluJL6--1ZsSIK70vVLPt5ZRClKjqr-MZMLykthRQHkdVncpsY0iSMt3ni0GZXzle4azkx_NiFhZeuZ4fh1HDF74EuC6Zk-VLw5FJ5f4xstQrCIZVW40
  [question-cmp-a_lgpl-l_propr.plantuml]: https://www.plantuml.com/plantuml/svg/RP9DQiCm48NtEiMGx0mseQMRa0HI4PT25e5kkr7iSInYIsAaD8HIw2dQc_H0EKbbDtKiujbfzBqtJp-bp16iGqBjJWQPtKjb18aQCbqRhL2v5tUg40vrTTVRjQN8zL6g7HIYidsTLDwj41a-Yco_8trGETUL9XYdRGsYcdAa2pPfYmr5WNS6leRmfhyaqiVRzZOCCfizRTRnVJ9PB63C-Uh1Pnh4rW6NffmFSnlIXYGwGQS6RK1l-S7ODVVIc4fcmacjE0WBe_ayQBqgy0jK5gSGHV-3esW4_HWzmxNSaf-tkfXroz2eb-pFAuZAklllx9vRloWIwleuNibODo2ki71nJp25y_Sd6D9l_YjP5XmhIKZkIfSaYq1mi6ZX_FN3VW40
  [question-cmp-a_propr-l_lgpl-l_propr.plantuml]: https://www.plantuml.com/plantuml/svg/bP9DRi8m48NtFiK8Tf68KAjk423H_5ILM21rsur99c0HR6jiYb1LgQTeRz83SP8QIfEOf8lERiPllZTZUoro16EHKDc3HcOsGcfEV0kPscebKTedUoWHTjlofd5grZnNUo5NKF3IDFk4TBI2PtZFiysAr4xcaIeLGJyzHwTKKOvqaGtFmRf6WLS6Bhhaf_gCIEtRvQK_o7Zyi9WFRiFXT0hzA4hkt4oTihcdIzCewjOjI6aIQ3aTJjAJq27V6AlddcbTYenReUGCk87UPEALdYMu1AN14GH10nW4FRXoQ63dOack5b-GLUhZVlVuOTLRiUh-Az3ZcHG4NDQFuzhQbeblcVnlWvPnyfSngmm69HPsq6WOmV7p7JIf5_U73QkM3qCIgxKDmwadtXaqSFpugeK-CMuHlHTgKsEF-Wq0
  [question-cmp-x_lgpl-l_propr.plantuml]: https://www.plantuml.com/plantuml/svg/TP91QiCm44NtEiMGx0mseQMRa0HQqvI25u4kkkb6iSUni2sPaT8GIg6dQ6_I0-KabPquaOaw6yFyf_-_BHSYGz062PNPDyXqAMJ3YTUGghfH4gLvCViAOLjNDuwg2vwfdP0Ro7cbtND2MhUSf_Z0qt93QYkpM5MAOBpineigof1EsAGRvgi27mpi-F1sVq-aTjVhzR38RFQqIiBRQB9Om3YE7-zi9o-M3BZbCexzt8fKGm8DftsBjg2r_6JiqhkrIaIAKcFsggW4hc4qd_S9q6lGYMyIgkDY2a7GGq4m0hUKO-yqICIQxECKu0ZGAqDtXpW63Dw6dQ_8a_7SXGG1bvURj1wCUUg45UOcz9fCuV3p1GsfT_ilQdRrgQA8nAOmKRJuv_XMeuR3zo_x0m00


## Related Questions

 - [opensource.stackexchange.com / GPL Application linked to a LGPL library loading proprietary-plugins](https://opensource.stackexchange.com/questions/9142/gpl-application-linked-to-a-lgpl-library-loading-proprietary-plugins)

  **consensus**:

  > No, what you propose is not legal.

 - [opensource.stackexchange.com / Linking from LGPL 2.1 software to Apache 2.0 library](https://opensource.stackexchange.com/questions/5664/linking-from-lgpl-2-1-software-to-apache-2-0-library)

  **consensus**:

  > Note that the LGPL-2.1 is permissive only with regards to software that uses this library ...

 - [opensource.stackexchange.com / GPL Application <-> LGPL Plugin <-> Proprietary code](https://opensource.stackexchange.com/questions/8505/gpl-application-lgpl-plugin-properietary-code)
 - [opensource.stackexchange.com / Can I use a proprietary library in my GPL'd program?](https://opensource.stackexchange.com/questions/2219/can-i-use-a-proprietary-library-in-my-gpld-program)

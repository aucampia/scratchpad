import logging
import random
import typing as t
import unittest
from contextlib import contextmanager
from http.server import BaseHTTPRequestHandler, HTTPServer, SimpleHTTPRequestHandler
from threading import Thread
from unittest.mock import MagicMock, call


def get_random_ip(parts: t.Optional[t.List[str]] = None) -> str:
    if not parts:
        parts = ["127"]
    for _index in range(4 - len(parts)):
        parts.append(f"{random.randint(0, 255)}")
    return ".".join(parts)


@contextmanager
def ctx_http_server(handler: t.Type[BaseHTTPRequestHandler]) -> t.Iterator[HTTPServer]:
    host = get_random_ip()
    # port = get_open_port(host)
    server = HTTPServer((host, 0), handler)
    server_thread = Thread(target=server.serve_forever)
    server_thread.daemon = True
    server_thread.start()
    logging.info("server: started ...")
    try:
        yield server
    finally:
        logging.info("server: will close ...")
        server.shutdown()
        server.socket.close()
        server_thread.join()


# CallableT = t.TypeVar("CallableT", bound=t.Callable[..., t.Any])
GenericT = t.TypeVar("GenericT")

# https://docs.python.org/3/library/unittest.mock-examples.html#mocking-unbound-methods

# https://stackoverflow.com/questions/50427397/spying-on-class-instantiation-and-methods
# https://stackoverflow.com/questions/25608107/python-mock-patching-a-method-without-obstructing-implementation/41599695#41599695


class UnboundMethodWrap:
    def __init__(self, func):
        self.func = func

    def __set_name__(self, obj, name):
        logging.info("entry: obj = %s, name = %s", obj, name)
        self.name = name
        # self.cls = obj

    def __get__(self, obj, objtype=None):
        logging.info("entry: obj = %s, objtype = %s", obj, objtype)

        def f(*args, **kwargs):
            return self.func(obj, *args, **kwargs)

        mock = MagicMock(wraps=f)
        setattr(obj, self.name, mock)
        return mock


def test_other() -> None:
    class A:
        @UnboundMethodWrap  # or do f = UnboundMethodWrap(f) later
        def f(self, x, y):
            print("called", self, x, y)
            return x + y

    logging.info("...")
    a = A()
    logging.info("...")
    print(a.f(2, 3))
    print(a.f("hello", "world"))
    logging.info("...")
    a.f.assert_has_calls([call(2, 3), call("hello", "world")])


class MethodSpy:
    mock: MagicMock
    method: t.Any

    def __init__(self):
        self.mock = MagicMock

    def __call__(self, slf: t.Any, *args: t.Any, **kwargs: t.Any) -> t.Any:
        self.mock(*args, **kwargs)
        return method(self, *args, **kwargs)  # type: ignore


def spy_decorator(method: GenericT) -> GenericT:
    mock = unittest.mock.MagicMock(spec=method)

    def wrapper(self: t.Any, *args: t.Any, **kwargs: t.Any) -> t.Any:
        mock(*args, **kwargs)
        return method(self, *args, **kwargs)  # type: ignore

    wrapper.mock = mock  # type: ignore
    return wrapper  # type: ignore


def test_something() -> None:
    logging.warning("0:entry ...")

    class Handler(SimpleHTTPRequestHandler):
        @MethodSpy
        def do_GET(self) -> None:
            logging.warning("do_GET: entry ...")
            self.send_response(200, "OK")
            self.end_headers()
            logging.warning("do_GET: end ...")


#     # HandlerSpec = Mock(spec=Handler)
#     # HandlerSpec = create_autospec(Handler)
#     # HandlerSpec = MagicMock(spec=Handler)
#     # hs = HandlerSpec()
#     # hs.do_GET()

#     # logging.info("Handler.do_GET = %s", Handler.do_GET)
#     # logging.info("Handler.do_GET.mock = %s", Handler.do_GET.mock)

#     with ctx_http_server(Handler) as server:
#         (host, port) = server.server_address
#         url = f"http://{host}:{port}/"
#         logging.info("url = %s", url)
#         # time.sleep(100)
#         response = request.urlopen(url)
#         # time.sleep(100)
#         assert response

#     assert Handler.do_GET.mock.assert_called()  # type: ignore


# class TestMock(unittest.TestCase):

if __name__ == "__main__":
    unittest.main()

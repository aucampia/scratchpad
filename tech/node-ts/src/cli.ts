#!/usr/bin/env node

/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-var-requires */

import pathm from "path";
const __file__ = pathm.basename(__filename);

const debugm = require('debug');
const debug = debugm(`tmpl:${__file__}`);
require('debug-trace')({patchOutput: false, overwriteDebugLog: console.error});
import consola from "consola";
(consola as any)._stdout = process.stderr;

import yargs from "yargs";
import * as cliMod from "./cliMod";

export async function main(): Promise<void> {
  const middleware = (args: yargs.Arguments<any>) => {
    consola.level += args.verbose;
    debug(`entry:`, {args, level: consola.level});
  }
  const parser = yargs
    .strict()
    .middleware([middleware])
    .help("h")
    .alias("help", "h")
    .option("verbose", {alias: "v", describe: "increase verbosity", count: true, default: 0})
    .command(cliMod)
    ;

  const parsed: any = parser.argv;
  consola.level += parsed.verbose;
  consola.debug(`entry ...`);
  consola.debug(`parsed = `, parsed);
}

main().catch((error) => {
  consola.error(`error =`, error);
  process.exitCode = 1;
});

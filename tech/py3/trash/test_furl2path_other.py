import os.path
from urllib.parse import unquote, urlparse
from urllib.request import url2pathname


def uri_to_path(uri, pathmod=os.path, url2pathname=url2pathname):
    parsed = urlparse(uri)
    host = "{0}{0}{mnt}{0}".format(pathmod.sep, mnt=parsed.netloc)
    return pathmod.normpath(pathmod.join(host, url2pathname(parsed.path)))


from unittest.mock import patch

import pytest


def file_uri_to_path2_(uri: str, pathmod=os.path, url2pathname=url2pathname):
    parsed = urlparse(uri)
    host = "{0}{0}{mnt}{0}".format(pathmod.sep, mnt=parsed.netloc)
    return pathmod.normpath(pathmod.join(host, url2pathname(parsed.path)))


def file_uri_to_path2(
    uri: str, path_class: Type[PurePathT] = pathlib.PurePath
) -> PurePathT:
    is_windows_path = isinstance(path_class(), pathlib.PureWindowsPath)
    return path_class(file_uri_to_path2_(uri))


@pytest.mark.parametrize(
    "uri, expected_path",
    [(r"file:///C:/Python27/Scripts/pip.exe", r"C:\Python27\Scripts\pip.exe")],
)
def test_windows(uri: str, expected_path: str) -> None:
    import ntpath

    # with patch("os.path.sep", ntpath.sep), patch(
    #     "os.path.normpath", ntpath.normpath
    # ), patch("os.path.join", ntpath.join):
    from nturl2path import url2pathname

    assert uri_to_path(uri, ntpath, url2pathname) == expected_path

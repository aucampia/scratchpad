# Response

## The purpose if the `id` attribute

The `id` attribute facilitates the use of short-form or short-hand XPointer as fragment identifiers in URIs:

[XML Schema Part 1: Structures > 3.15.2.2 References to Schema Components from Elsewhere](https://www.w3.org/TR/xmlschema-1/#d0e16826)

> The names of schema components such as type definitions and element declarations are not of type ID: they are not unique within a schema, just within a symbol space. This means that simple fragment identifiers will not always work to reference schema components from outside the context of schema documents.
>
> [...]
>
> Short-form fragment identifiers may also be used in some cases, that is when a DTD or XML Schema is available for the schema in question, and the provision of an `id` attribute for the representations of all primary and secondary schema components, which _is_ of type ID, has been exploited.

[RFC 3986: Uniform Resource Identifier (URI): Generic Syntax > 3.5. Fragment ](https://tools.ietf.org/html/rfc3986#section-3.5)

> The fragment's format and resolution is therefore
> dependent on the media type [RFC2046] of a potentially retrieved
> representation, even though such a retrieval is only performed if the
> URI is dereferenced.  

[RFC 7303: XML Media Types > 5. Fragment Identifiers](https://tools.ietf.org/html/rfc7303#section-5)

> The syntax and semantics of fragment identifiers for the XML media
> types defined in this specification are based on the
> [XPointerFramework] W3C Recommendation.

[XPointer Framework > 3.2 Shorthand Pointer](https://www.w3.org/TR/xptr-framework/#shorthand)

> A shorthand pointer, formerly known as a barename, consists of an NCName alone. It identifies at most one element in the resource's information set; specifically, the first one (if any) in document order that has a matching NCName as an identifier. The identifiers of an element are determined as follows:
>
> 1. If an element information item has an attribute information item among its __[attributes]__ that is a schema-determined ID, then it is identified by the value of that attribute information item's [schema normalized value] property;

## Referencing other schema components by `id`

It is not possible to refer to other schema components (which includes XSD types) by id within XSD itself. The only semantics specified in XSD for referencing other schema components is via QName. For example:

[XML Schema Part 1: Structures > 3.3.2 XML Representation of Element Declaration Schema Components](https://www.w3.org/TR/xmlschema-1/#declare-element)

> ```
> <element
> ```
> [...]
> ```
>   ref = QName
>   substitutionGroup = QName
>   type = QName
> ```

QName is defined as follow in XML Schema:

[XML Schema Part 2: Datatypes > 3.2.18 QName](https://www.w3.org/TR/xmlschema-2/#QName)

> [Definition:] QName represents XML qualified names. The ·value space· of QName is the set of tuples {namespace name, local part}, where namespace name is an anyURI and local part is an NCName. The ·lexical space· of QName is the set of strings that ·match· the QName production of [Namespaces in XML].

> __Note__:  The mapping between literals in the ·lexical space· and values in the ·value space· of QName requires a namespace declaration to be in scope for the context in which QName is used.

[Namespaces in XML 1.0 > 4 Qualified Names](https://www.w3.org/TR/REC-xml-names/#ns-qualnames)

> ```
> [7]     QName             ::= PrefixedName
>                               | UnprefixedName
> [8]     PrefixedName      ::= Prefix ':' LocalPart
> [9]     UnprefixedName    ::= LocalPart
> ```

And for resolution, it can be seen from 

[XML Schema Part 1: Structures > 3.15.3 Constraints on XML Representations of Schemas](https://www.w3.org/TR/xmlschema-1/#src-resolve)

> ### Schema Representation Constraint: QName resolution (Schema Document)
> For a ·QName· to resolve to a schema component of a specified kind all of the following must be true:

> [...]

> 2 The component's {name} matches the ·local name· of the ·QName·;

> [...]

And _{name}_ refers to the `name` attribute of components:

[XML Schema Part 1:  > 3.2.2 XML Representation of Attribute Declaration Schema Components](https://www.w3.org/TR/xmlschema-1/#declare-attribute)

> {name}  The ·actual value· of the `name` [attribute]

[XML Schema Part 1:  > 3.3.2 XML Representation of Element Declaration Schema Components](https://www.w3.org/TR/xmlschema-1/#declare-element)

> {name}  The ·actual value· of the `name` [attribute]

[XML Schema Part 1:  > 3.4.2 XML Representation of Complex Type Definitions](https://www.w3.org/TR/xmlschema-1/#declare-type)

> {name} The ·actual value· of the `name` [attribute] if present, otherwise ·absent·.

etc...

#!/usr/bin/env python3
# vim: set filetype=python sts=4 ts=4 sw=4 expandtab tw=100 cc=+1:

import logging
logging.basicConfig(level=logging.INFO, datefmt="%Y-%m-%dT%H:%M:%S",
    format=("%(asctime)s %(process)d %(thread)d %(levelno)03d:%(levelname)-8s "
        "%(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s"))
LOGGER = logging.getLogger(__name__)

import os, sys; sys.path.append(os.path.dirname(os.path.realpath(__file__)))
LOGGER.info("import dmlink_ok: direct")
import dmlink_ok

def module_entry():
    LOGGER.info("entry")
    dmlink_ok.module_entry()

def main():
    LOGGER.info("entry")
    module_entry()

if __name__ == "__main__":
    main()

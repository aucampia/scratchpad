# ...

```bash
https://github.com/BFO-ontology/BFO2/blob/master/ontology/src/bfo.owl

pip3 install --user --upgrade rdflib rdflib-jsonld
rdfpipe -i application/rdf+xml -o json-ld https://raw.githubusercontent.com/BFO-ontology/BFO2/master/ontology/src/bfo.owl > bfo.jsonld
```

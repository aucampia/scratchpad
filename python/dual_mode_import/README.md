# ...

```
$ ./dmlink_ok.py
2020-01-28T10:33:47 24526 139878680733248 020:INFO     __main__     dmlink_ok:11:<module> import file_a: direct
2020-01-28T10:33:47 24526 139878680733248 020:INFO     __main__     dmlink_ok:22:main entry
2020-01-28T10:33:47 24526 139878680733248 020:INFO     __main__     dmlink_ok:18:module_entry entry
2020-01-28T10:33:47 24526 139878680733248 020:INFO     file_a       file_a:13:module_entry entry
```

```
$ ./dmlink_nok.py
2020-01-28T10:33:49 24535 140546056083008 020:INFO     __main__     dmlink_nok:10:<module> import file_a: relative
Traceback (most recent call last):
  File "./dmlink_nok.py", line 11, in <module>
    from . import file_a
ImportError: cannot import name 'file_a'
```

```
$ ./final_ok.py
2020-01-28T10:34:15 24591 140628817196608 020:INFO     __main__     final_ok:11:<module> import file_a: direct
2020-01-28T10:34:15 24591 140628817196608 020:INFO     dmlink_ok    dmlink_ok:14:<module> import file_a: relative
Traceback (most recent call last):
  File "./final_ok.py", line 12, in <module>
    import dmlink_ok
  File "/home/E1258170/syncthing/sw-wpw/d/gitlab.com/aucampia/scratchpads/general-2001/python/dual_mode_import/dmlink_ok.py", line 15, in <module>
    from . import file_a
ImportError: attempted relative import with no known parent package
```

```
$ ./final_nok.py
2020-01-28T10:34:30 24632 140178486134336 020:INFO     __main__     final_nok:10:<module> import file_a: relative
2020-01-28T10:34:30 24632 140178486134336 020:INFO     dmlink_nok   dmlink_nok:10:<module> import file_a: relative
Traceback (most recent call last):
  File "./final_nok.py", line 11, in <module>
    import dmlink_nok
  File "/home/E1258170/syncthing/sw-wpw/d/gitlab.com/aucampia/scratchpads/general-2001/python/dual_mode_import/dmlink_nok.py", line 11, in <module>
    from . import file_a
ImportError: attempted relative import with no known parent package
```

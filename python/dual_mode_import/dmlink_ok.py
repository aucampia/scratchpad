#!/usr/bin/env python3
# vim: set filetype=python sts=4 ts=4 sw=4 expandtab tw=100 cc=+1:

import logging
logging.basicConfig(level=logging.INFO, datefmt="%Y-%m-%dT%H:%M:%S",
    format=("%(asctime)s %(process)d %(thread)d %(levelno)03d:%(levelname)-8s "
        "%(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s"))
LOGGER = logging.getLogger(__name__)

LOGGER.info("import file_a: direct")
import file_a

def module_entry():
    LOGGER.info("entry")
    file_a.module_entry()

def main():
    LOGGER.info("entry")
    module_entry()

if __name__ == "__main__":
    main()

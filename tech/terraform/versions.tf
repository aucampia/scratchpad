# https://www.terraform.io/docs/language/settings/index.html
# https://www.terraform.io/docs/language/expressions/version-constraints.html
terraform {
  required_providers {
    null = {
      source = "hashicorp/null"
      version = "~> 3.1.0"
    }
    tls = {
      source = "hashicorp/tls"
      version = "~> 3.1.0"
    }
    local = {
      source = "hashicorp/local"
      version = "~> 2.1.0"
    }
  }
  required_version = ">= 1.0.0, < 2.0.0"
}

import React from "react";
import "./App.css";
import SparqlClient from "sparql-http-client";

export const client = new SparqlClient({
  endpointUrl: "https://query.wikidata.org/sparql",
});

export function useSparqlQuery(query: string) {
  console.log(`useSparqlQuery: query =`, query);

  const cid = Math.random();
  console.log(`useSparqlQuery:${cid}: query =`, query);

  const [loading, setLoading] = React.useState(true);
  const [error, setError] = React.useState(null as Error | null);
  const [data, setData] = React.useState(null as any[] | null);
  React.useEffect(() => {
    setError(null);
    setLoading(true);
    (async () => {
      console.log(`useSparqlQuery:useEffect:${cid}:entry ...`);
      try {
        const stream = await client!.query.select(query, {
          operation: "postUrlencoded"
        });
        const newData = [];
        for await (const row of stream) {
          newData.push(row);
        }
        console.log(`useSparqlQuery:useEffect:${cid}:newData =`, newData);
        setData(newData);
      } catch (error) {
        setError(error);
      } finally {
        console.log(`useSparqlQuery:useEffect:${cid}: finally ...`);
        setLoading(false);
      }
    })();
  }, [query]);
  console.log(
    `useSparqlQuery:${cid}: loading=${loading}, error=${error}, data =`,
    data
  );
  if (!loading && !data) throw new Error(`BAD STATE ...`);

  return { loading, error, data };
}

function App() {
  const catsQ = useSparqlQuery(`
  SELECT ?item ?itemLabel WHERE {
    ?item wdt:P31 wd:Q146.
    SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
  } LIMIT 1
  `);
  const goatsQ = useSparqlQuery(`
  SELECT ?item ?itemLabel WHERE {
    ?item wdt:P31 wd:Q2934.
    SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }
  } LIMIT 1
  `);
  return (
    <div className="App">
      <div>
        <p>Cats ...</p>
        <pre>
          {catsQ.loading ? "loading" : JSON.stringify(catsQ.data, null, "  ")}
        </pre>
        <p>Goats ...</p>
        <pre>
          {goatsQ.loading ? "loading" : JSON.stringify(goatsQ.data, null, "  ")}
        </pre>
      </div>
    </div>
  );
}

export default App;

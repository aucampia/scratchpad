# ...

```bash
plantuml-to-url.sh svg answer-*.plantuml
plantuml-to-url.sh svg question-*.plantuml
redcarpet --parse tasklist --render tasklist --smarty question.md > question.html
redcarpet --parse tasklist --render tasklist --smarty answer.md > answer.html
```


## scratch

```
Both GPL and LGPL ...

This statement is however a simplficiation of a more nuanced license.
```

"You may place library facilities that are a work based on the Library side by side in a single library together with other library facilities that are not Applications and are not covered by this License, and convey such a combined library under terms of your choice" ~ https://www.gnu.org/licenses/lgpl-3.0-standalone.html#section5

To me this would make most sense if the operation of "placing two library facilities side by side" was commutative. At the very least there is nothing in the text to indicate that such an operation is noncommutative. And of Option A and Option B presented here under the "LGPL-3.0 / Section 5: Combined Libraries." heading here: https://opensource.stackexchange.com/a/9313/16951 ...

Only the operation that results in Option A is commutative, the operation that results in Option B is noncommutative.  So while I cannot say definitively "placing two library facilities side by side" must be a commutative operation, if it was then it would rule out option B.

Is relevant to the actions in question ...

Option B is similar to function composition.

https://en.wikipedia.org/wiki/Commutative_property
https://en.wikipedia.org/wiki/Symmetric_function
```

## terms

```
https://en.wikipedia.org/wiki/Jargon#Industry_term

https://en.wiktionary.org/wiki/term_of_art
https://en.wiktionary.org/wiki/technical_term

https://www.dictionary.com/browse/term-of-art

term of art, jargon, technical term,

https://en.wikipedia.org/wiki/Standing_(law)

https://en.wikipedia.org/wiki/Contractual_term
```

## sources

```
https://www.gnu.org/licenses/gpl-3.0-standalone.html
https://www.gnu.org/licenses/gpl-3.0-standalone.html#preamble
https://www.gnu.org/licenses/gpl-3.0-standalone.html#section0

https://www.gnu.org/licenses/lgpl-3.0-standalone.html
https://www.gnu.org/licenses/lgpl-3.0-standalone.html#section0
```

## resources

```
https://www.gnu.org/licenses/license-list.en.html

https://www.gnu.org/licenses/gpl-faq.html#AllCompatibility

https://opensource.org/licenses/alphabetical

https://choosealicense.com/licenses/
https://choosealicense.com/licenses/lgpl-3.0/
https://choosealicense.com/licenses/gpl-3.0/

https://tldrlegal.com/license/gnu-lesser-general-public-license-v3-(lgpl-3)
https://tldrlegal.com/license/gnu-general-public-license-v3-(gpl-3)
```

```
https://en.wikipedia.org/wiki/Open-source_license
https://en.wikipedia.org/wiki/Public-domain-equivalent_license
https://en.wikipedia.org/wiki/Comparison_of_free_and_open-source_software_licenses
https://en.wikipedia.org/wiki/License_compatibility
https://en.wikipedia.org/wiki/Contractual_term
https://en.wikipedia.org/wiki/Standing_(law)
```

## Excerpts

### GPL-3.0

### LGPL-3.0


### GPL FAQ

> #### Who has the power to enforce the GPL? (#WhoHasThePower) ([#WhoHasThePower][GPLFAQ#WhoHasThePower])
> Since the GPL is a copyright license, the copyright holders of the software are the ones who have the power to enforce the GPL. If you see a violation of the GPL, you should inform the developers of the GPL-covered software involved. They either are the copyright holders, or are connected with the copyright holders. Learn more about reporting GPL violations.

> #### Is the developer of a GPL-covered program bound by the GPL? Could the developer's actions ever be a violation of the GPL? ([#DeveloperViolate][GPLFAQ#DeveloperViolate])
> Strictly speaking, the GPL is a license from the developer for others to use, distribute and change the program. The developer itself is not bound by it, so no matter what the developer does, this is not a “violation” of the GPL.
> However, if the developer does something that would violate the GPL if done by someone else, the developer will surely lose moral standing in the community.

 [GPLFAQ#DeveloperViolate]: https://www.gnu.org/licenses/gpl-faq.en.html#DeveloperViolate
 [GPLFAQ#WhoHasThePower]: https://www.gnu.org/licenses/gpl-faq.html#WhoHasThePower

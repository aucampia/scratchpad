# type: ignore

from unittest.mock import MagicMock, call

# https://stackoverflow.com/questions/50427397/spying-on-class-instantiation-and-methods
# https://stackoverflow.com/questions/25608107/python-mock-patching-a-method-without-obstructing-implementation/41599695#41599695


class UnboundMethodWrap:
    def __init__(self, func):
        self.func = func

    def __set_name__(self, obj, name):
        self.name = name

    def __get__(self, obj, objtype=None):
        def f(*args, **kwargs):
            return self.func(self, *args, **kwargs)

        mock = MagicMock(wraps=f)
        setattr(obj, self.name, mock)
        return mock


class A:
    @UnboundMethodWrap  # or do f = UnboundMethodWrap(f) later
    def f(self, x, y):
        print("called", self, x, y)
        return x + y


a = A()
print(a.f(2, 3))
print(a.f("hello", "world"))
a.f.assert_has_calls([call(2, 3), call("hello", "world")])

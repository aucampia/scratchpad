from _pytest.capture import CaptureFixture

from aucampia.scratchpad.subprocess import (
    OSSubprocessHandler,
    args_to_str,
    make_run_args_dict,
)


def test_handler(capfd: CaptureFixture[str]) -> None:
    handler = OSSubprocessHandler()
    handler.run(make_run_args_dict(["echo", "WORK .*.*. ING"]))
    captured = capfd.readouterr()
    assert captured.out == "WORK .*.*. ING\n"


def test_facade(capfd: CaptureFixture[str]) -> None:
    facade = OSSubprocessHandler.facade(True, True)
    args = ["echo", "this & is * the . string"]
    facade.run(args)
    captured = capfd.readouterr()
    assert captured.err == f"{args_to_str(args)}\n"
    assert captured.out == f"{args[1]}\n"

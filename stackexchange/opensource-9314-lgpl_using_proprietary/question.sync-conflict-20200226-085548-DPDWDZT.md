# What permissions does LGPL-3.0 grant if the licensed work links against a proprietary library?

According to the FSF ["The Lesser GPL permits use of the library in proprietary program"][WNL].

Both [GPL-3.0][GPL-3.0] and [LGPL-3.0][LGPL-3.0] are grants of specific
permissions ("rights") from a copyright holder of a work to other parties,
or "Licensees".

Some of the permissions granted by these licenses have specific conditions
which must be met for the permission grant to be in effect.
If these conditions are not met then the permission is essentially not granted.

This question seeks to clarify whether the requiresite conditions
for any of the permissions granted by LGPL-3.0 is met if a LGPL-3.0 licensed work
links against a proprietary library.

In this question "propritary library" refers to a library that is
NOT licensed under a LGPL-3.0 and GPL-3.0 compatible license and which, additionally, is
NOT licensed under a license that is considered an open source or free software license.

More specifically then, may a *licensee* of a LGPL-3.0 work that links against a
propritary library do any of the following with the work licensed under LGPL-3.0:

* [convey modified source versions](GPL-3.0#S4)
* [convey non-source forms](GPL-3.0#S5)

And if so, under what conditions.

Consider the following components:

 - `ApplicationA` **Proprietary** licensed
 - `ApplicationB` **LGPL-3** licensed
 - `ApplicationC` **LGPL-3** licensed
 - `LibraryD` **LGPL-3** licensed
 - `LibraryE` **Proprietary** licensed

In the following arrangements:

 - **AA:Proprietary ⟶ LD:LGPL3 ⟶ LE:Proprietary**:

  `ApplicationA` (**Proprietary**) links against `LibraryD` (**LGPL-3**) which links against `LibraryE` (**Proprietary**)

 - **AB:LGPL3 ⟶ LD:LGPL3 ⟶ LE:Proprietary** with **AB:LGPL3** and **LD:LGPL3** being distributed:

  `ApplicationB` (**LGPL-3**) links against `LibraryD` (**LGPL-3**) which links against `LibraryE` (**Proprietary**)



The question here is are the conditions neccesary for any of

What I would like clarity

What I would like to have clarity on is whether the LGPL also allows a program, application or library that is LGPL licensed to use a proprietary library.

Key to answering this question is whether LGPL cares about directionality of linking.

Say I have the following:


Are any the following scenarios in violation of LGPL-3:

 - **AA:Proprietary ⟶ LD:LGPL3 ⟶ LE:Proprietary** with **LD:LGPL3** being distributed:

  `ApplicationA` (**Proprietary**) links against `LibraryD` (**LGPL-3**) which links against `LibraryE` (**Proprietary**)

  [![enter image description here][1]][1]

 - **AB:LGPL3 ⟶ LD:LGPL3 ⟶ LE:Proprietary** with **AB:LGPL3** and **LD:LGPL3** being distributed:

  `ApplicationB` (**LGPL-3**) links against `LibraryD` (**LGPL-3**) which links against `LibraryE` (**Proprietary**)

  [![enter image description here][2]][2]

 - **AC:LGPL3 ⟶ LE:Proprietary** with **AC:LGPL3** being distributed:

  `ApplicationC` (**LGPL-3**) links against `LibraryE` (**Proprietary**)

  [![enter image description here][3]][3]


[I guess to some extent the answer would depends on who is doing it][GPLF-DeveloperViolate]:

> **Is the developer of a GPL-covered program bound by the GPL? Could the developer's actions ever be a violation of the GPL?**

> Strictly speaking, the GPL is a license from the developer for others to use, distribute and change the program. The developer itself is not bound by it, so no matter what the developer does, this is not a “violation” of the GPL.

> However, if the developer does something that would violate the GPL if done by someone else, the developer will surely lose moral standing in the community.

My current understanding on the matter:

 - **AC:LGPL3 ⟶ LE:Proprietary**

  This would lead to potential license violations of LGPL-3 because if the ApplicationC was GPL-3 it would not be allowed as per [gpl-faq/MoneyGuzzlerInc][GPLF-MoneyGuzzlerInc]. And as far as I understand LGPL-3 only modifies GPL-3 in terms of what links to the work covered under LGPL-3, not what that work links to. That is directionality of linking is importation.

  From [LGPL 3.0](https://www.gnu.org/licenses/lgpl-3.0.en.html)

  > A “Combined Work” is a work produced by combining or **linking an Application with the Library**.

  So to be clear, the text does not include something to the effect of "**linking the Library with another work**". This to me seems like the directionality of linking could be relevant.

  Violations will occur when someone who is not the copyright holder of ApplicationC uses it and they will be liable to the copyright holder of ApplicationC

 - **AA:Proprietary ⟶ LD:LGPL3 ⟶ LE:Proprietary** and **AB:LGPL3 ⟶ LD:LGPL3 ⟶ LE:Proprietary**

  Basically same as **AC:LGPL3 ⟶ LE:Proprietary**.

The following seems to be somewhat related

 - [opensource.stackexchange.com / GPL Application linked to a LGPL library loading proprietary-plugins](https://opensource.stackexchange.com/questions/9142/gpl-application-linked-to-a-lgpl-library-loading-proprietary-plugins)

  **consensus**: No, what you propose is not legal.

 - [opensource.stackexchange.com / Linking from LGPL 2.1 software to Apache 2.0 library](https://opensource.stackexchange.com/questions/5664/linking-from-lgpl-2-1-software-to-apache-2-0-library)

  **consensus**: "Note that the LGPL-2.1 is permissive only with regards to software that uses this library ..."

 - [opensource.stackexchange.com / GPL Application <-> LGPL Plugin <-> Properietary code](https://opensource.stackexchange.com/questions/8505/gpl-application-lgpl-plugin-properietary-code)
 - [opensource.stackexchange.com / Can I use a proprietary library in my GPL'd program?](https://opensource.stackexchange.com/questions/2219/can-i-use-a-proprietary-library-in-my-gpld-program)

  [1]: https://www.plantuml.com/plantuml/svg/VS_12i8m30RWitcAl41FtWJ71WulFUmLQXTdiBObZGSHtzqAYax1uvzy_25jKITwFWK8j5VKXCBJGN5a8Q-S8e0FhXJiSWxitMFKuHKMDL0iX6iS96LXgZqNk3srvPtKl9bBknsiMNwZ_byTpEuQqxohLnWu7j5DZcDHRE2r-MJzBsifZlNXEm00
  [2]: https://www.plantuml.com/plantuml/svg/ZSynhi8m30RW-VaKla2djo5H5KJ5ag5NCAaf5Y6973CWnDq94WWg5aRx_sJhxugHslaK8F3Uq1AgJ0V3KPIzIOe0Fb0fkCevYAV7QerN-Ai3nyAuHBSTNFCFjoTqibFIo-ONrC_He2chiDMaIfYzR9hsVNg1GU8HQIA9nR25L_B9-c_MSHnhrpi0
  [3]: https://www.plantuml.com/plantuml/svg/LOx1oW8n38JFVvuYBz3JVnDSLaIyzB2l4Bjn3TQs9F4WuhjRGP0zpimtmupcf7w_PSXyTlIAAil5SHRbv58BGCfaXhlMiYJwM7jymbyNN8nnY_4unV0FhoyOvQIaZyCQchGs5VQUT19MQo4ClzO6ivGhqa9Ip760aSlS_xq1
  [GPLF-DeveloperViolate]: https://www.gnu.org/licenses/gpl-faq.en.html#DeveloperViolate
  [GPLF-MoneyGuzzlerInc]: https://www.gnu.org/licenses/gpl-faq.en.html#MoneyGuzzlerInc
  [WNL]: https://www.gnu.org/licenses/why-not-lgpl.html
  [GPL-3.0]: https://www.gnu.org/licenses/gpl-3.0-standalone.html
  [LGPL-3.0]: https://www.gnu.org/licenses/lgpl-3.0-standalone.html
  [GPL-3.0#S4]: https://www.gnu.org/licenses/gpl-3.0-standalone.html#section4
  [GPL-3.0#S5]: https://www.gnu.org/licenses/gpl-3.0-standalone.html#section5

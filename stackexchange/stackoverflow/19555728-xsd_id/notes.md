# answer

* [XML Schema Part 0: Primer](https://www.w3.org/TR/xmlschema-0/)
* [XML Schema Part 1: Structures](https://www.w3.org/TR/xmlschema-1/)
  * [3 Schema Component Details](https://www.w3.org/TR/xmlschema-1/#components)
    * [3.3 Element Declarations](https://www.w3.org/TR/xmlschema-1/#cElement_Declarations)
      * [3.3.2 XML Representation of Element Declaration Schema Components](https://www.w3.org/TR/xmlschema-1/#declare-element)
* [XML Schema Part 2: Datatypes](https://www.w3.org/TR/xmlschema-2/)


## trash

* https://www.w3.org/TR/xmlschema-0/#import
* https://www.w3.org/TR/xmlschema-0/#ref5
* https://www.w3.org/TR/xmlschema-1/#src-resolve
* https://www.w3.org/TR/xmlschema-1/#cvc-resolve-instance
* https://www.w3.org/TR/xmlschema-1/#d0e16860
* https://www.w3.org/TR/xmlschema-1/#Element_Declaration
* https://www.w3.org/TR/xmlschema-1/#refSchemaConstructs

> <element>s within <schema> produce global element declarations; <element>s within <group> or <complexType> produce either particles which contain global element declarations (if there's a ref attribute) or local declarations (otherwise). For complete declarations, top-level or local, the type attribute is used when the declaration can use a built-in or pre-declared type definition. Otherwise an anonymous <simpleType> or <complexType> is provided inline.


## 1.0

https://www.w3.org/2001/XMLSchema.xsd
http://www.w3.org/XML/XMLSchema/v1.0
https://www.w3.org/TR/xmlschema-0/
https://www.w3.org/TR/xmlschema-1/
https://www.w3.org/TR/xmlschema-2/

## 1.1

http://www.w3.org/XML/XMLSchema/v1.1
https://www.w3.org/2009/XMLSchema/XMLSchema.xsd
https://www.w3.org/TR/xmlschema11-1/
https://www.w3.org/TR/xmlschema11-2/

## ...

* https://github.com/eclipse-ee4j/jaxb-ri
* https://openjdk.java.net/jeps/320#Java-EE-modules
* https://stackoverflow.com/questions/48204141/replacements-for-deprecated-jpms-modules-with-java-ee-apis/48204154#48204154
* https://repo1.maven.org/maven2/com/sun/xml/bind/jaxb-ri/2.3.0.1/jaxb-ri-2.3.0.1.zip
* https://stackoverflow.com/questions/52618325/how-to-run-xjc-in-java-9

* https://javaee.github.io/jaxb-v2/doc/user-guide/index.html
* https://github.com/javaee/jaxb-v2
* https://github.com/eclipse-ee4j/jaxb-ri
* https://repository.jboss.org/nexus/content/repositories/public/javax/activation/javax.activation/1.1.0.v201005080500/javax.activation-1.1.0.v201005080500.jar
* https://mvnrepository.com/artifact/com.sun.xml.bind/jaxb-ri/2.4.0-b180830.0438
* http://central.maven.org/maven2/com/sun/xml/bind/jaxb-ri/2.4.0-b180830.0438/

* https://github.com/apache/xerces2-j/blob/Xerces-J_2_12_0/src/org/apache/xerces/impl/xs/traversers/XSDHandler.java


##

> Seemingly every tag in XSD has an optional ID attribute that can be specified, but what is the point?

* https://www.w3.org/TR/xml/#sec-attribute-types
** 


[3.15.2 XML Representations of Schemas](https://www.w3.org/TR/xmlschema-1/#declare-schema)

> The other attributes (id and version) are for user convenience, and this specification defines no semantics for them.

https://lists.w3.org/Archives/Public/www-xml-schema-comments/2000JulSep/0251.html

> Section 4.3.10: XML Representation of Annotation Schema components

> Almost every other element has an optional "id" attribute, it would be
> useful if annotation, appinfo and documentation also had them.  Specially in
> the case of documentation elements, having an "id" attribute makes it easier
> to support external documentation files and multiple language documentation.
> It can, of course, be done with external subsets or attributes from other
> namespaces, but it would be preferable to have it as part of the schema for
> schemas.

https://lists.w3.org/Archives/Public/www-xml-schema-comments/2007JulSep/0207.html

> thank you very much for regarding my proposal.

> So I propose to define such a referencing mechanism to a type  
> definition within a Schema via its id attribute.

https://lists.w3.org/Archives/Public/www-xml-schema-comments/2000OctDec/0093.html

```
We have responded to this request by adding an 'id' attribute of type
ID to all datatype and facet defining elements in the XML Schema
document for schemas/datatypes (currently at
http://www.w3.org/2000/10/datatypes.xsd, but moving at CR to http://www.w3.org/2000/10/XMLSchema.xsd).  This means that the stable URIs you requested are of the form

  http://www.w3.org/2000/10/XMLSchema#[datatype or facet name]

e.g.

  http://www.w3.org/2000/10/XMLSchema#decimal
  http://www.w3.org/2000/10/XMLSchema#precision

It would be helpful to us to know whether you are satisfied with the
decision taken by the WG on this issue, or wish your dissent from the
WG's decision to be recorded for consideration by the Director of
the W3C.
```


https://lists.w3.org/Archives/Public/www-xml-schema-comments/2000AprJun/0181.html

```
It will be a significant benefit to the wider XML application community
if canonical URIs were defined for each of the built-in datatypes as well
as for each the specified facets.
```

https://lists.w3.org/Archives/Public/www-xml-schema-comments/2000AprJun/0210.html

```
In principle, which would you prefer:

  1) http://www.w3.org/TR/xmlschema-2/#timeDuration

    Already exists, resolves to section in HTML document

  2) http://www.w3.org/1999/XMLSchema#timeDuration

    Doesn't exist yet, but can easily be made to,
    Resolves to element in the Schema for Schemas.
```

https://www.w3.org/DesignIssues/Fragment.html

```
```

* https://www.w3.org/TR/xptr-framework/

```
This specification defines the XML Pointer Language (XPointer) Framework, an extensible system for XML addressing that underlies additional XPointer scheme specifications. The framework is intended to be used as a basis for fragment identifiers for any resource whose Internet media type is one of text/xml, application/xml, text/xml-external-parsed-entity, or application/xml-external-parsed-entity. Other XML-based media types are also encouraged to use this framework in defining their own fragment identifier languages.
```

* https://www.w3.org/TR/xptr-framework/#shorthand


[3.5.  Fragment](https://tools.ietf.org/html/rfc3986#section-3.5)

```
```


[Multipurpose Internet Mail Extensions (MIME) Part Two: Media Types](https://tools.ietf.org/html/rfc2046)


https://www.w3.org/TR/xml-id/


https://www.w3.org/2006/02/son-of-3023/draft-murata-kohn-lilley-xml-04.html#frag

https://www.w3.org/2005/04/xpointer-schemes/

http://lists.xml.org/archives/xml-dev/
https://lists.w3.org/Archives/Public/xmlschema-dev/
https://lists.w3.org/Archives/Public/www-xml-schema-comments/

## Answer

IDs were introduced in 

On 2000-05-12 someone requested on xml-schema-comments mailing list: ["Please make well-known URIs for all built-in datatypes and facets"](https://lists.w3.org/Archives/Public/www-xml-schema-comments/2000AprJun/0181.html)

> It will be a significant benefit to the wider XML application community
> if canonical URIs were defined for each of the built-in datatypes as well
> as for each the specified facets.

In response to that [](https://lists.w3.org/Archives/Public/www-xml-schema-comments/2000OctDec/0093.html)

[The Cambridge Communiqué > 3. Observations and Recommendations](https://www.w3.org/TR/1999/NOTE-schema-arch-19991007#observations)

>  9. XML Schema type hierarchies and RDF type hierarchies are not the same and need not be unified; in particular, it is too soon to tell if RDF schemas can leverage XML Schema archetypes. However the atomic data types, notably URIref, should be shared and work needs to be done to support this.


* https://www.w3.org/TR/xml-infoset/

```
2.2.1 Type Definition Components
During ·validation·, [Definition:]  declaration components are associated by (qualified) name to information items being ·validated·.

2.2.2 Declaration Components

There are three kinds of declaration component: element, attribute, and notation. Each is described in a section below. Also included is a discussion of element substitution groups, which is a feature provided in conjunction with element declarations.

2.2 XML Schema Abstract Data Model

2.5 Names and Symbol Spaces

3.4.4 Complex Type Definition Validation Rules???

3.3.1 The Element Declaration Schema Component

A {scope} of global identifies element declarations available for use in content models throughout the schema. Locally scoped declarations are available for use only within the complex type identified by the {scope} property. This property is ·absent· in the case of declarations within named model groups: their scope is determined when they are used in the construction of complex type definitions.

3.3.2 XML Representation of Element Declaration Schema Components
As noted above the names for top-level element declarations are in a separate

<element>s within <schema> produce global element declarations; <element>s within <group> or <complexType> produce either particles which contain global element declarations (if there's a ref attribute) or local declarations (otherwise). For complete declarations, top-level or local, the type attribute is used when the declaration can use a built-in or pre-declared type definition. Otherwise an anonymous <simpleType> or <complexType> is provided inline.

2.3.2 The element declaration is top-level (i.e. its {scope} is global), {abstract} is false, the element information item's [namespace name] is identical to the element declaration's {target namespace} (where an ·absent· {target namespace} is taken to be identical to a [namespace name] with no value) and the element information item's [local name] matches the element declaration's {name}.
In this case the element declaration is the ·context-determined declaration· for the element information item with respect to Schema-Validity Assessment (Element) (§3.3.4) and Assessment Outcome (Element) (§3.3.5).

2.3.3 The element declaration is top-level (i.e. its {scope} is global), its {disallowed substitutions} does not contain substitution, the [local ] and [namespace name] of the element information item resolve to an element declaration, as defined in QName resolution (Instance) (§3.15.4) -- [Definition:]  call this declaration the substituting declaration and the ·substituting declaration· together with the particle's element declaration's {disallowed substitutions} is validly substitutable for the particle's element declaration as defined in Substitution Group OK (Transitive) (§3.3.6).
In this case the ·substituting declaration· is the ·context-determined declaration· for the element information item with respect to Schema-Validity Assessment (Element) (§3.3.4) and Assessment Outcome (Element) (§3.3.5).


Schema Representation Constraint: QName resolution (Schema Document)
For a ·QName· to resolve to a schema component of a specified kind all of the following must be true:
1 That component is a member of the value of the appropriate property of the schema which corresponds to the schema document within which the ·QName· appears, that is the appropriate case among the following must be true:
1.1 If the kind specified is simple or complex type definition, then the property is the {type definitions}.
1.2 If the kind specified is attribute declaration, then the property is the {attribute declarations}.
1.3 If the kind specified is element declaration, then the property is the {element declarations}.
1.4 If the kind specified is attribute group, then the property is the {attribute group definitions}.
1.5 If the kind specified is model group, then the property is the {model group definitions}.
1.6 If the kind specified is notation declaration, then the property is the {notation declarations}.
2 The component's {name} matches the ·local name· of the ·QName·;
3 The component's {target namespace} is identical to the ·namespace name· of the ·QName·;
4 The appropriate case among the following must be true:
4.1 If the ·namespace name· of the ·QName· is ·absent·, then one of the following must be true:
4.1.1 The <schema> element information item of the schema document containing the ·QName· has no targetNamespace [attribute].
4.1.2 The <schema> element information item of the that schema document contains an <import> element information item with no namespace [attribute].
4.2 otherwise the ·namespace name· of the ·QName· is the same as one of the following:
4.2.1 The ·actual value· of the targetNamespace [attribute] of the <schema> element information item of the schema document containing the ·QName·.
4.2.2 The ·actual value· of the namespace [attribute] of some <import> element information item contained in the <schema> element information item of that schema document.


3.15.2.2 References to Schema Components from Elsewhere

A fragment identifier of the form #xpointer(xs:schema/xs:element[@name="person"]) will uniquely identify the representation of a top-level element declaration with name person, and similar fragment identifiers can obviously be constructed for the other global symbol spaces.

https://www.w3.org/TR/xml-infoset/#infoitem.element
[children] An ordered list of child information items, in document order. This list contains element, processing instruction, unexpanded entity reference, character, and comment information items, one for each element, processing instruction, reference to an unprocessed external entity, data character, and comment appearing immediately within the current element. If the element is empty, this list has no members.

2.2.3 Naming Conflicts
We have now described how to define new complex types (e.g. PurchaseOrderType), declare elements (e.g. purchaseOrder) and declare attributes (e.g. orderDate). These activities generally involve naming, and so the question naturally arises: What happens if we give two things the same name? The answer depends upon the two things in question, although in general the more similar are the two things, the more likely there will be a conflict.

Here are some examples to illustrate when same names cause problems. If the two things are both types, say we define a complex type called USStates and a simple type called USStates, there is a conflict. If the two things are a type and an element or attribute, say we define a complex type called USAddress and we declare an element called USAddress, there is no conflict. If the two things are elements within different types (i.e. not global elements), say we declare one element called name as part of the USAddress type and a second element called name as part of the Item type, there is no conflict. (Such elements are sometimes called local element declarations.) Finally, if the two things are both types and you define one and XML Schema has defined the other, say you define a simple type called decimal, there is no conflict. The reason for the apparent contradiction in the last example is that the two types belong to different namespaces. We explore the use of namespaces in schema in a later section.
```

## Answer

[3.15.2.2 References to Schema Components from Elsewhere](https://www.w3.org/TR/xmlschema-1/#d0e16826)

> The names of schema components such as type definitions and element declarations are not of type ID: they are not unique within a schema, just within a symbol space. This means that simple fragment identifiers will not always work to reference schema components from outside the context of schema documents.

> ...

> Short-form fragment identifiers may also be used in some cases, that is when a DTD or XML Schema is available for the schema in question, and the provision of an `id` attribute for the representations of all primary and secondary schema components, which _is_ of type ID, has been exploited.

[XPointer Framework > 3 Language and Processing > 3.2 Shorthand Pointer](https://www.w3.org/TR/xptr-framework/#shorthand)

> A shorthand pointer, formerly known as a barename, consists of an NCName alone. It identifies at most one element in the resource's information set; specifically, the first one (if any) in document order that has a matching NCName as an identifier. The identifiers of an element are determined as follows:

> 1. If an element information item has an attribute information item among its __[attributes]__ that is a schema-determined ID, then it is identified by the value of that attribute information item's [schema normalized value] property;

[3.3.2 XML Representation of Element Declaration Schema Components](https://www.w3.org/TR/xmlschema-1/#declare-element)

[3.2.18 QName](https://www.w3.org/TR/xmlschema-2/#QName)

[3.3.7 NCName](https://www.w3.org/TR/xmlschema-2/#NCName)

[3.15.1 The Schema Itself](https://www.w3.org/TR/xmlschema-1/#Schema_details)

> __`{type definitions}`__
>
>   A set of named simple and complex type definitions.
>
> __`{attribute declarations}`__
>
>   A set of named (top-level) attribute declarations.
>
> __`{element declarations}`__
>
>   A set of named (top-level) element declarations.
>
> __`{attribute group definitions}`__
>
>   A set of named attribute group definitions.
>
> __`{model group definitions}`__
>
>   A set of named model group definitions.
>
> __`{notation declarations}`__
>
>   A set of notation declarations.
>
> __`{annotations}`__
>
>   A set of annotations.

[Validation Rule: QName resolution (Instance)](https://www.w3.org/TR/xmlschema-1/#cvc-resolve-instance)
[Schema Representation Constraint: QName resolution (Schema Document)](https://www.w3.org/TR/xmlschema-1/#src-resolve)


[3.15.2 XML Representations of Schemas](https://www.w3.org/TR/xmlschema-1/#declare-schema)

> The other attributes (id and version) are for user convenience, and this specification defines no semantics for them.


[2.2.2 Global Elements & Attributes](https://www.w3.org/TR/xmlschema-0/#Globals)

> Global elements, and global attributes, are created by declarations that appear as the children of the schema element. Once declared, a global element or a global attribute can be referenced in one or more declarations using the ref attribute as described above. A declaration that references a global element enables the referenced element to appear in the instance document in the context of the referencing declaration. So, for example, the comment element appears in po.xml at the same level as the shipTo, billTo and items elements because the declaration that references comment appears in the complex type definition at the same level as the declarations of the other three elements.


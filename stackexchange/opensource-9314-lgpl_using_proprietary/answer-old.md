# Answer

As a starting point it is critical to note that **GPL-3.0** prohibits the conveyance of a the "object code" or modified "source code" of **GPL-3.0** licensed works that uses a proprietary library.

Stated differently, the conveyance of "object code" or modified "source code" of **LD:LGPL3** or **AB:LGPL3** from the example would be prohibited by license to anybody except their copyright owners if the licenses were **GPL-3.0** instead of **LGPL-3.0**.

More on this here: [Licensing software that uses a proprietary library under GPL-3.0 without linking exceptions: what are the implications?](https://opensource.stackexchange.com/questions/9314/licensing-software-that-uses-a-proprietary-library-under-gpl-3-0-without-linking)

Then it is also important to note that [**LGPL-3.0**][LGPL-3.0] is an extension of [**GPL-3.0**][GPL-3.0]:

> This version of the GNU Lesser General Public License incorporates the terms and conditions of version 3 of the GNU General Public License, supplemented by the additional permissions listed below.

So we can modify the question here to: would the additional permissions granted by **LGPL-3.0** eliminate the aforementioned prohibitions from **GPL-3.0** in any of the example scenarios.

## Conclusion (Reasoning follows)

The only additional permission that is applicable to the example arrangements is the ability to upgrade to a later version of LGPL. Consequently all the prohibitions from **GPL-3.0** is still in force.

## Regarding Directionality:

**LGPL-3.0** defines the following terms [**LGPL-3.0** / Section 0: Additional Definitions.][LGPL-3.0-S0]:

> 1. **“The Library”** refers to a covered work governed by this License, other than **an Application** or a **Combined Work** as defined below.

> 2. **An “Application”** is any work that makes use of **an interface** provided by the **Library**, but which is not otherwise based on **the Library**. Defining a subclass of a class defined by **the Library** is deemed a mode of using an interface provided by **the Library**.

> 3. **A “Combined Work”** is a work produced by combining or linking **an Application** with **the Library**. The particular version of **the Library** with which **the Combined Work** was made is also called the “Linked Version”.

These definitions are then in the arrangement that can be seen in this diagram:

[![Permitted LGPL-3.0 Arrangement][LGPL-3.0-S0-UCD]][LGPL-3.0-S0-UCD]

**Figure 1: LGPL Arrangement ▲**

In this arrangement the following is clearly directional:

> makes use of **an interface** provided by **the Library**

In the examples however we don't have something (i.e. another work)
«_[making] use of **an interface** provided by **the Library**_».

Instead in the examples arrangements there is something (i.e. another work, proprietary) «_providing **an interface** used by **the Library**_» as can be seen in this diagram:

[![Example Arrangement][EXAMPLE-UCD]][EXAMPLE-UCD]

**Figure 2: Example Arrangement ▲**

This arrangement is not described by the **LGPL-3.0** definitions and any additional permissions granted by **LGPL-3.0** that are contingent on those definitions are therefore also not available in the example arrangements.

We can simplify this question then by eliminating all sections and corresponding permissions contingent on definitions that do not occur in the examples and then only consider sections that remain.

## Sections and Permissions contingent on arrangements that do not occur:

### [**LGPL-3.0** / Section 1: Exception to Section 3 of the GNU GPL.][LGPL-3.0-S1]

This is section applies only if section 3 and 4 is applicable and as is shown below they are not.

### [**LGPL-3.0** / Section 2: Conveying Modified Versions.][LGPL-3.0-S2]

> If you modify a copy of the Library, and, in your modifications, a facility refers to a function or data to be supplied by **an Application** that uses the facility [...], then you may [...]

The permissions granted by this section is contingent on the definition of **An “Application”** and since that arrangement does not occur in the example arrangements the permissions granted by this section are also not available in the example arrangements.

### [**LGPL-3.0** / Section 3: Object Code Incorporating Material from Library Header Files.][LGPL-3.0-S3]

> The object code form of **an Application** may incorporate material from a header file that is part of **the Library**. You may convey such object code under terms of your choice [...]

Same as section 2, not valid as this section is contingent on the definition of **An “Application”**.

### [**LGPL-3.0** / Section 4: Combined Works.][LGPL-3.0-S4]

> You may convey **a Combined Work** under terms of your choice [...]

The definition of **A "Combined Work"** is contingent on the definition of **An “Application”** and that definition does not come into play since there is no such arrangement in the examples. Consequently the definition of **A "Combined Work"** cannot come into play either so the permissions granted by this section are not available in the example arrangements.

## Remaining sections

This elimination process leaves only Section 5 and Section 6:

### [**LGPL-3.0** / Section 5: Combined Libraries.][LGPL-3.0-S5]

> You may place library facilities that are a work based on the Library side by side in a single library together with other library facilities that are not Applications and are not covered by this License, and convey such a combined library under terms of your choice, if [...]

This is somewhat unclear, what would be placing library facilities "side by side" and what would not be placing library facilities "side by side"? We don't really have well defined geometry of libraries or library facilities here.

We can however scavenge some snippers from the **LGPL-3.0** to try and construct one as best we can:




> [...] place library facilities that are a work based on the Library side by side in a single library [...] ~ [**LGPL-3.0** / Section 5][LGPL-3.0-S5]

> [...] an Application that uses the facility [...] ~ [**LGPL-3.0** / Section 2][LGPL-3.0-S2]

From this we can posit:

* A library can have one or more facility.
* Two library facilities can be arranged in a manner which we can say they are side-by-side.
* Two library facilities can be arranged in a manner which we can say they are not side-by-side.
* A library facility can be used.

We can maybe also propose:

* A library facility can use another library facility.

So then we can consider:

[![side by side][s5-option_a]][s5-option_a]

**Figure 3: Option A ▲**

[![not side by side][s5-option_b]][s5-option_b]

**Figure 4: Option B ▲**

In these figures "LGPL Library Facility" is shorthand for "library facilities that are a work based on the Library".

And we can then extend these options with the additional library facilities to get:

[![side by side][s5-combined_a]][s5-combined_a]

**Figure 5: combined library: Option A ▲**

[![not side by side][s5-combined_b]][s5-combined_b]

**Figure 6: combined library: Option B ▲**

To me **side by side** seems to favour Option A rather than Option B.

I definitely do not think that side-by-side is clearly Option B and think that side-by-side is more likely option A than option B.

On this basis I would favour the view that permissions granted by section 5 is also not applicable as option A is not similar to the example arrangements.

### [**LGPL-3.0** / Section 6: Revised Versions of the GNU Lesser General Public License.][LGPL-3.0-S6]

This section clearly still applies, though the only permission it grants it the ability to use later versions of LGPL with some conditions.


 [GPL-3.0]: https://www.gnu.org/licenses/gpl-3.0-standalone.html
 [LGPL-3.0]: https://www.gnu.org/licenses/lgpl-3.0-standalone.html
 [LGPL-3.0-S0]: https://www.gnu.org/licenses/lgpl-3.0-standalone.html#section0
 [LGPL-3.0-S1]: https://www.gnu.org/licenses/lgpl-3.0-standalone.html#section1
 [LGPL-3.0-S2]: https://www.gnu.org/licenses/lgpl-3.0-standalone.html#section2
 [LGPL-3.0-S3]: https://www.gnu.org/licenses/lgpl-3.0-standalone.html#section3
 [LGPL-3.0-S4]: https://www.gnu.org/licenses/lgpl-3.0-standalone.html#section4
 [LGPL-3.0-S5]: https://www.gnu.org/licenses/lgpl-3.0-standalone.html#section5
 [LGPL-3.0-S6]: https://www.gnu.org/licenses/lgpl-3.0-standalone.html#section6
 [LGPL-3.0-S0-UCD]: https://www.plantuml.com/plantuml/svg/PP1DQiCm48NtEiMWPS2YjBiiIamNfU15e8LkifdOaqJO7edHf61AeQTeRz83vIILmI4_sKdlwVjuQ94KHVTzPrBhEQ9W3tNeOs1YVTMX8yZblJ4HwnQt16yxUWhzsZCrxq5Qm0HsDiifs6CEOv532vy6JhOHhVnQK8Q9iYCsHVaAArunTBucJZI7wdbP5G_kpbv8IYvZp6zGVU39KpBOi_HMjPGGnPEY3AFDiv9iiAPiUpcU9nVogSuBZO7ptKL76oqki3aSVhyXIlZm3IK3LmkT4x_TgNEFLzG-KOB3pv_vCWlY9l_oFm00
 [EXAMPLE-UCD]: https://www.plantuml.com/plantuml/svg/RO_HIiCm58RlynG_lT90Y-ZTBcHLYmZ55JS4OJUnE-l2aZGagJX4y2dqJNoWFOcT5hlgxaxEp_VzEMCVX0kDLiolfR722OsorhOsPC8qR1IX3KyPiw9SYuemhJKLtImy8iwpPw6j8gJE2LEHRa7ESSJvxMI6oybzTfTTuU81-NMHStuSuOMXxy1iHRbyTC9jEb_KRj2jsYVcPcwWP4d6q-ZR4fyb9z50aXQkjavIQ9cXJS3s4THFzi-wX-_4qWHoIr4IKdFpEtT2qyUSHupXO7kiQ1dY0J_2zkDjzvqdkI3Fi7zwaZXPhKAIdFz16ayUs_TFnbxPcCoYqUeB

 [s5-option_a]: https://raw.githubusercontent.com/aucampia/for-stackexchange/master/opensource-9314-lgpl_using_proprietary/opensource-9314-lgpl_using_proprietary-option_a.svg?sanitize=true
 [s5-option_b]: https://raw.githubusercontent.com/aucampia/for-stackexchange/master/opensource-9314-lgpl_using_proprietary/opensource-9314-lgpl_using_proprietary-option_b.svg?sanitize=true
 [s5-combined_a]: https://raw.githubusercontent.com/aucampia/for-stackexchange/master/opensource-9314-lgpl_using_proprietary/opensource-9314-lgpl_using_proprietary-combined_a.svg?sanitize=true
 [s5-combined_b]: https://raw.githubusercontent.com/aucampia/for-stackexchange/master/opensource-9314-lgpl_using_proprietary/opensource-9314-lgpl_using_proprietary-combined_b.svg?sanitize=true

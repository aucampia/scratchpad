import random
import socket
import time
from typing import List, Optional, cast


def get_random_ip(parts: Optional[List[str]] = None) -> str:
    if parts is None:
        parts = ["127"]
    for _ in range(4 - len(parts)):
        parts.append(f"{random.randint(0, 255)}")
    return ".".join(parts)


def get_random_port() -> int:
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.bind(("", 0))
        sock.listen(1)
        port = sock.getsockname()[1]
        sock.close()
    return cast(int, port)


def wait_for_port(host: str, port: int, timeout: float = 5) -> None:
    start_time = time.perf_counter()

    def remaining_time() -> float:
        return timeout - (time.perf_counter() - start_time)

    while remaining_time() > 0:
        try:
            with socket.create_connection((host, port), timeout=remaining_time()):
                return
        except OSError:
            time.sleep(1)
    raise TimeoutError(f"timed out when waiting {timeout} seconds for {host}:{port}")
